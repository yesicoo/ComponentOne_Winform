﻿namespace ControlExplorer.Barcode
{
    partial class Overview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Overview));
            this.c1InputPanel1 = new C1.Win.C1InputPanel.C1InputPanel();
            this.lblValue = new C1.Win.C1InputPanel.InputLabel();
            this.txtValue = new C1.Win.C1InputPanel.InputTextBox();
            this.hdrTypes = new C1.Win.C1InputPanel.InputGroupHeader();
            this.c1BarcodeHost39 = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lbl39 = new C1.Win.C1InputPanel.InputLabel();
            this.c1BarcodeHost93 = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lbl93 = new C1.Win.C1InputPanel.InputLabel();
            this.c1BarcodeHosti2of5 = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lbli2of5 = new C1.Win.C1InputPanel.InputLabel();
            this.c1BarcodeHostCodabar = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lblCodabar = new C1.Win.C1InputPanel.InputLabel();
            this.c1BarcodeHost128 = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lbl128 = new C1.Win.C1InputPanel.InputLabel();
            this.c1BarcodeHostPostNet = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lblPostNet = new C1.Win.C1InputPanel.InputLabel();
            this.c1BarcodeHostEAN8 = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lblEAN8 = new C1.Win.C1InputPanel.InputLabel();
            this.c1BarcodeHostEAN13 = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lblEAN13 = new C1.Win.C1InputPanel.InputLabel();
            this.c1BarcodeHostUpcA = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lblUpcA = new C1.Win.C1InputPanel.InputLabel();
            this.c1BarcodeHostUpcE = new ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost();
            this.lblUpcE = new C1.Win.C1InputPanel.InputLabel();
            this.c1SuperTooltip1 = new C1.Win.C1SuperTooltip.C1SuperTooltip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.c1InputPanel1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1InputPanel1
            // 
            this.c1InputPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1InputPanel1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.c1InputPanel1.Items.Add(this.lblValue);
            this.c1InputPanel1.Items.Add(this.txtValue);
            this.c1InputPanel1.Items.Add(this.hdrTypes);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHost39);
            this.c1InputPanel1.Items.Add(this.lbl39);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHost93);
            this.c1InputPanel1.Items.Add(this.lbl93);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHosti2of5);
            this.c1InputPanel1.Items.Add(this.lbli2of5);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHostCodabar);
            this.c1InputPanel1.Items.Add(this.lblCodabar);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHost128);
            this.c1InputPanel1.Items.Add(this.lbl128);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHostPostNet);
            this.c1InputPanel1.Items.Add(this.lblPostNet);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHostEAN8);
            this.c1InputPanel1.Items.Add(this.lblEAN8);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHostEAN13);
            this.c1InputPanel1.Items.Add(this.lblEAN13);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHostUpcA);
            this.c1InputPanel1.Items.Add(this.lblUpcA);
            this.c1InputPanel1.Items.Add(this.c1BarcodeHostUpcE);
            this.c1InputPanel1.Items.Add(this.lblUpcE);
            this.c1InputPanel1.Location = new System.Drawing.Point(0, 0);
            this.c1InputPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1InputPanel1.Name = "c1InputPanel1";
            this.c1InputPanel1.Padding = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.c1InputPanel1.Size = new System.Drawing.Size(867, 626);
            this.c1InputPanel1.TabIndex = 1;
            this.c1InputPanel1.ToolTipSettings.AutomaticDelay = 0;
            this.c1InputPanel1.ToolTipSettings.Images.Add(new C1.Win.C1InputPanel.ImageEntry("TraceError.png", ((System.Drawing.Image)(resources.GetObject("c1InputPanel1.ToolTipSettings.Images")))));
            this.c1InputPanel1.ToolTipSettings.InitialDelay = 200;
            this.c1InputPanel1.ToolTipSettings.IsBalloon = true;
            this.c1InputPanel1.ToolTipSettings.MaximumWidth = 300;
            this.c1InputPanel1.ToolTipSettings.ReshowDelay = 200;
            // 
            // lblValue
            // 
            this.lblValue.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValue.Name = "lblValue";
            this.lblValue.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.lblValue.Text = "条码值: ";
            // 
            // txtValue
            // 
            this.txtValue.Name = "txtValue";
            this.txtValue.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.txtValue.Text = "12345";
            this.txtValue.Width = 150;
            this.txtValue.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
            // 
            // hdrTypes
            // 
            this.hdrTypes.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hdrTypes.Name = "hdrTypes";
            this.hdrTypes.Text = "支持的编码类型";
            // 
            // c1BarcodeHost39
            // 
            this.c1BarcodeHost39.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHost39.BarHeight = 40;
            this.c1BarcodeHost39.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHost39.CodeType = C1.Win.C1BarCode.CodeTypeEnum.Code39;
            this.c1BarcodeHost39.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHost39.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHost39.Height = 55;
            this.c1BarcodeHost39.Name = "c1BarcodeHost39";
            this.c1BarcodeHost39.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHost39.ShowText = true;
            this.c1BarcodeHost39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHost39.Text = "12345";
            this.c1BarcodeHost39.Width = 250;
            // 
            // lbl39
            // 
            this.lbl39.Break = C1.Win.C1InputPanel.BreakType.Row;
            this.lbl39.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl39.FontPadding = true;
            this.lbl39.Name = "lbl39";
            this.lbl39.Text = "39";
            this.lbl39.ToolTipText = resources.GetString("lbl39.ToolTipText");
            // 
            // c1BarcodeHost93
            // 
            this.c1BarcodeHost93.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHost93.BarHeight = 40;
            this.c1BarcodeHost93.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHost93.CodeType = C1.Win.C1BarCode.CodeTypeEnum.Code93;
            this.c1BarcodeHost93.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHost93.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHost93.Height = 55;
            this.c1BarcodeHost93.Name = "c1BarcodeHost93";
            this.c1BarcodeHost93.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHost93.ShowText = true;
            this.c1BarcodeHost93.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHost93.Text = "12345";
            this.c1BarcodeHost93.Width = 250;
            // 
            // lbl93
            // 
            this.lbl93.Break = C1.Win.C1InputPanel.BreakType.Row;
            this.lbl93.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl93.FontPadding = true;
            this.lbl93.Name = "lbl93";
            this.lbl93.Text = "93";
            this.lbl93.ToolTipText = resources.GetString("lbl93.ToolTipText");
            // 
            // c1BarcodeHosti2of5
            // 
            this.c1BarcodeHosti2of5.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHosti2of5.BarHeight = 40;
            this.c1BarcodeHosti2of5.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHosti2of5.CodeType = C1.Win.C1BarCode.CodeTypeEnum.CodeI2of5;
            this.c1BarcodeHosti2of5.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHosti2of5.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHosti2of5.Height = 55;
            this.c1BarcodeHosti2of5.Name = "c1BarcodeHosti2of5";
            this.c1BarcodeHosti2of5.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHosti2of5.ShowText = true;
            this.c1BarcodeHosti2of5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHosti2of5.Text = "12345";
            this.c1BarcodeHosti2of5.Width = 250;
            // 
            // lbli2of5
            // 
            this.lbli2of5.Break = C1.Win.C1InputPanel.BreakType.Row;
            this.lbli2of5.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbli2of5.FontPadding = true;
            this.lbli2of5.Name = "lbli2of5";
            this.lbli2of5.Text = "i2of5";
            this.lbli2of5.ToolTipText = resources.GetString("lbli2of5.ToolTipText");
            // 
            // c1BarcodeHostCodabar
            // 
            this.c1BarcodeHostCodabar.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHostCodabar.BarHeight = 40;
            this.c1BarcodeHostCodabar.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHostCodabar.CodeType = C1.Win.C1BarCode.CodeTypeEnum.Codabar;
            this.c1BarcodeHostCodabar.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHostCodabar.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHostCodabar.Height = 55;
            this.c1BarcodeHostCodabar.Name = "c1BarcodeHostCodabar";
            this.c1BarcodeHostCodabar.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHostCodabar.ShowText = true;
            this.c1BarcodeHostCodabar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHostCodabar.Text = "12345";
            this.c1BarcodeHostCodabar.Width = 250;
            // 
            // lblCodabar
            // 
            this.lblCodabar.Break = C1.Win.C1InputPanel.BreakType.Row;
            this.lblCodabar.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblCodabar.FontPadding = true;
            this.lblCodabar.Name = "lblCodabar";
            this.lblCodabar.Text = "Codabar";
            this.lblCodabar.ToolTipText = resources.GetString("lblCodabar.ToolTipText");
            // 
            // c1BarcodeHost128
            // 
            this.c1BarcodeHost128.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHost128.BarHeight = 40;
            this.c1BarcodeHost128.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHost128.CodeType = C1.Win.C1BarCode.CodeTypeEnum.Code128;
            this.c1BarcodeHost128.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHost128.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHost128.Height = 55;
            this.c1BarcodeHost128.Name = "c1BarcodeHost128";
            this.c1BarcodeHost128.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHost128.ShowText = true;
            this.c1BarcodeHost128.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHost128.Text = "12345";
            this.c1BarcodeHost128.Width = 250;
            // 
            // lbl128
            // 
            this.lbl128.Break = C1.Win.C1InputPanel.BreakType.Column;
            this.lbl128.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl128.FontPadding = true;
            this.lbl128.Name = "lbl128";
            this.lbl128.Text = "128";
            this.lbl128.ToolTipText = resources.GetString("lbl128.ToolTipText");
            // 
            // c1BarcodeHostPostNet
            // 
            this.c1BarcodeHostPostNet.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHostPostNet.BarHeight = 40;
            this.c1BarcodeHostPostNet.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHostPostNet.CodeType = C1.Win.C1BarCode.CodeTypeEnum.PostNet;
            this.c1BarcodeHostPostNet.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHostPostNet.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHostPostNet.Height = 55;
            this.c1BarcodeHostPostNet.Name = "c1BarcodeHostPostNet";
            this.c1BarcodeHostPostNet.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHostPostNet.ShowText = true;
            this.c1BarcodeHostPostNet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHostPostNet.Text = "12345";
            this.c1BarcodeHostPostNet.Width = 250;
            // 
            // lblPostNet
            // 
            this.lblPostNet.Break = C1.Win.C1InputPanel.BreakType.Row;
            this.lblPostNet.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPostNet.FontPadding = true;
            this.lblPostNet.Name = "lblPostNet";
            this.lblPostNet.Text = "PostNet";
            this.lblPostNet.ToolTipText = resources.GetString("lblPostNet.ToolTipText");
            // 
            // c1BarcodeHostEAN8
            // 
            this.c1BarcodeHostEAN8.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHostEAN8.BarHeight = 40;
            this.c1BarcodeHostEAN8.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHostEAN8.CodeType = C1.Win.C1BarCode.CodeTypeEnum.Ean8;
            this.c1BarcodeHostEAN8.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHostEAN8.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHostEAN8.Height = 55;
            this.c1BarcodeHostEAN8.Name = "c1BarcodeHostEAN8";
            this.c1BarcodeHostEAN8.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHostEAN8.ShowText = true;
            this.c1BarcodeHostEAN8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHostEAN8.Text = "12345";
            this.c1BarcodeHostEAN8.Width = 250;
            // 
            // lblEAN8
            // 
            this.lblEAN8.Break = C1.Win.C1InputPanel.BreakType.Row;
            this.lblEAN8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEAN8.FontPadding = true;
            this.lblEAN8.Name = "lblEAN8";
            this.lblEAN8.Text = "EAN-8";
            this.lblEAN8.ToolTipText = resources.GetString("lblEAN8.ToolTipText");
            // 
            // c1BarcodeHostEAN13
            // 
            this.c1BarcodeHostEAN13.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHostEAN13.BarHeight = 40;
            this.c1BarcodeHostEAN13.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHostEAN13.CodeType = C1.Win.C1BarCode.CodeTypeEnum.Ean13;
            this.c1BarcodeHostEAN13.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHostEAN13.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHostEAN13.Height = 55;
            this.c1BarcodeHostEAN13.Name = "c1BarcodeHostEAN13";
            this.c1BarcodeHostEAN13.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHostEAN13.ShowText = true;
            this.c1BarcodeHostEAN13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHostEAN13.Text = "12345";
            this.c1BarcodeHostEAN13.Width = 250;
            // 
            // lblEAN13
            // 
            this.lblEAN13.Break = C1.Win.C1InputPanel.BreakType.Row;
            this.lblEAN13.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblEAN13.FontPadding = true;
            this.lblEAN13.Name = "lblEAN13";
            this.lblEAN13.Text = "EAN-13";
            this.lblEAN13.ToolTipText = resources.GetString("lblEAN13.ToolTipText");
            // 
            // c1BarcodeHostUpcA
            // 
            this.c1BarcodeHostUpcA.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHostUpcA.BarHeight = 40;
            this.c1BarcodeHostUpcA.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHostUpcA.CodeType = C1.Win.C1BarCode.CodeTypeEnum.UpcA;
            this.c1BarcodeHostUpcA.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHostUpcA.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHostUpcA.Height = 55;
            this.c1BarcodeHostUpcA.Name = "c1BarcodeHostUpcA";
            this.c1BarcodeHostUpcA.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHostUpcA.ShowText = true;
            this.c1BarcodeHostUpcA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHostUpcA.Text = "12345";
            this.c1BarcodeHostUpcA.Width = 250;
            // 
            // lblUpcA
            // 
            this.lblUpcA.Break = C1.Win.C1InputPanel.BreakType.Row;
            this.lblUpcA.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpcA.FontPadding = true;
            this.lblUpcA.Name = "lblUpcA";
            this.lblUpcA.Text = "UPC-A";
            this.lblUpcA.ToolTipText = resources.GetString("lblUpcA.ToolTipText");
            // 
            // c1BarcodeHostUpcE
            // 
            this.c1BarcodeHostUpcE.BackColor = System.Drawing.Color.White;
            this.c1BarcodeHostUpcE.BarHeight = 40;
            this.c1BarcodeHostUpcE.Break = C1.Win.C1InputPanel.BreakType.None;
            this.c1BarcodeHostUpcE.CodeType = C1.Win.C1BarCode.CodeTypeEnum.UpcE;
            this.c1BarcodeHostUpcE.Font = new System.Drawing.Font("Courier New", 8F);
            this.c1BarcodeHostUpcE.ForeColor = System.Drawing.Color.Black;
            this.c1BarcodeHostUpcE.Height = 55;
            this.c1BarcodeHostUpcE.Name = "c1BarcodeHostUpcE";
            this.c1BarcodeHostUpcE.Padding = new System.Windows.Forms.Padding(2);
            this.c1BarcodeHostUpcE.ShowText = true;
            this.c1BarcodeHostUpcE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.c1BarcodeHostUpcE.Text = "12345";
            this.c1BarcodeHostUpcE.Width = 250;
            // 
            // lblUpcE
            // 
            this.lblUpcE.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblUpcE.FontPadding = true;
            this.lblUpcE.Name = "lblUpcE";
            this.lblUpcE.Text = "UPC-E";
            this.lblUpcE.ToolTipText = resources.GetString("lblUpcE.ToolTipText");
            // 
            // c1SuperTooltip1
            // 
            this.c1SuperTooltip1.Font = new System.Drawing.Font("Tahoma", 8F);
            // 
            // Overview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(867, 626);
            this.Controls.Add(this.c1InputPanel1);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "Overview";
            this.Text = "Overview";
            this.Load += new System.EventHandler(this.Overview_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c1InputPanel1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1InputPanel.C1InputPanel c1InputPanel1;
        private C1.Win.C1InputPanel.InputTextBox txtValue;
        private C1.Win.C1InputPanel.InputLabel lblValue;
        private C1.Win.C1SuperTooltip.C1SuperTooltip c1SuperTooltip1;
        private C1.Win.C1InputPanel.InputGroupHeader hdrTypes;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHost39;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHost93;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHosti2of5;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHostCodabar;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHost128;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHostPostNet;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHostEAN8;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHostEAN13;
        private C1.Win.C1InputPanel.InputLabel lbl39;
        private C1.Win.C1InputPanel.InputLabel lbl93;
        private C1.Win.C1InputPanel.InputLabel lbli2of5;
        private C1.Win.C1InputPanel.InputLabel lblCodabar;
        private C1.Win.C1InputPanel.InputLabel lbl128;
        private C1.Win.C1InputPanel.InputLabel lblPostNet;
        private C1.Win.C1InputPanel.InputLabel lblEAN8;
        private C1.Win.C1InputPanel.InputLabel lblEAN13;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHostUpcA;
        private C1.Win.C1InputPanel.InputLabel lblUpcA;
        private ControlExplorer.InputPanel.ControlHosts.C1BarcodeHost c1BarcodeHostUpcE;
        private C1.Win.C1InputPanel.InputLabel lblUpcE;
    }
}