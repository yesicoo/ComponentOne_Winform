﻿namespace ControlExplorer.GanttView
{
    partial class GanttViewDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn1 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn2 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn3 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn4 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn5 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn6 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn7 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn8 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn9 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn10 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn11 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn12 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn13 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.TaskPropertyColumn taskPropertyColumn14 = new C1.Win.C1GanttView.TaskPropertyColumn();
            C1.Win.C1GanttView.Task task1 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task2 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task3 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task4 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task5 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task6 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task7 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task8 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task9 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task10 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task11 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task12 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task13 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task14 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task15 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task16 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task17 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task18 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task19 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task20 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task21 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task22 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task23 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task24 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task25 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task26 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task27 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task28 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task29 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task30 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task31 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task32 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task33 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task34 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task35 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task36 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task37 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task38 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task39 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task40 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task41 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task42 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task43 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task44 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task45 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task46 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task47 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task48 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task49 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task50 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task51 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task52 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task53 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task54 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task55 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task56 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task57 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task58 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task59 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task60 = new C1.Win.C1GanttView.Task();
            C1.Win.C1GanttView.Task task61 = new C1.Win.C1GanttView.Task();
            this.c1GanttView1 = new C1.Win.C1GanttView.C1GanttView();
            ((System.ComponentModel.ISupportInitialize)(this.c1GanttView1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1GanttView1
            // 
            this.c1GanttView1.BackColor = System.Drawing.SystemColors.Window;
            taskPropertyColumn1.Caption = "任务名称";
            taskPropertyColumn1.ID = 601931593;
            taskPropertyColumn1.Property = C1.Win.C1GanttView.TaskProperty.Name;
            taskPropertyColumn2.Caption = "计划模式";
            taskPropertyColumn2.ID = 587421;
            taskPropertyColumn2.Property = C1.Win.C1GanttView.TaskProperty.Mode;
            taskPropertyColumn3.Caption = "持续时间";
            taskPropertyColumn3.ID = 375365303;
            taskPropertyColumn3.Property = C1.Win.C1GanttView.TaskProperty.Duration;
            taskPropertyColumn3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            taskPropertyColumn3.Visible = false;
            taskPropertyColumn4.Caption = "单位";
            taskPropertyColumn4.ID = 469359217;
            taskPropertyColumn4.Property = C1.Win.C1GanttView.TaskProperty.DurationUnits;
            taskPropertyColumn4.Visible = false;
            taskPropertyColumn5.Caption = "开始";
            taskPropertyColumn5.ID = 1312026651;
            taskPropertyColumn5.Property = C1.Win.C1GanttView.TaskProperty.Start;
            taskPropertyColumn5.Visible = false;
            taskPropertyColumn6.Caption = "结束";
            taskPropertyColumn6.ID = 300927924;
            taskPropertyColumn6.Property = C1.Win.C1GanttView.TaskProperty.Finish;
            taskPropertyColumn6.Visible = false;
            taskPropertyColumn7.Caption = "完成百分比";
            taskPropertyColumn7.ID = 927242872;
            taskPropertyColumn7.Property = C1.Win.C1GanttView.TaskProperty.PercentComplete;
            taskPropertyColumn7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            taskPropertyColumn7.Visible = false;
            taskPropertyColumn8.Caption = "约束类型";
            taskPropertyColumn8.ID = 338690695;
            taskPropertyColumn8.Property = C1.Win.C1GanttView.TaskProperty.ConstraintType;
            taskPropertyColumn8.Visible = false;
            taskPropertyColumn9.Caption = "约束日期";
            taskPropertyColumn9.ID = 99080331;
            taskPropertyColumn9.Property = C1.Win.C1GanttView.TaskProperty.ConstraintDate;
            taskPropertyColumn9.Visible = false;
            taskPropertyColumn10.Caption = "前驱任务";
            taskPropertyColumn10.ID = 1201109715;
            taskPropertyColumn10.Property = C1.Win.C1GanttView.TaskProperty.Predecessors;
            taskPropertyColumn10.Visible = false;
            taskPropertyColumn11.Caption = "截止日期";
            taskPropertyColumn11.ID = 1188446638;
            taskPropertyColumn11.Property = C1.Win.C1GanttView.TaskProperty.Deadline;
            taskPropertyColumn11.Visible = false;
            taskPropertyColumn12.Caption = "日历";
            taskPropertyColumn12.ID = 1681723345;
            taskPropertyColumn12.Property = C1.Win.C1GanttView.TaskProperty.Calendar;
            taskPropertyColumn12.Visible = false;
            taskPropertyColumn13.Caption = "资源名称";
            taskPropertyColumn13.ID = 1493350636;
            taskPropertyColumn13.Property = C1.Win.C1GanttView.TaskProperty.ResourceNames;
            taskPropertyColumn13.Visible = false;
            taskPropertyColumn14.Caption = "标注";
            taskPropertyColumn14.ID = 1299767733;
            taskPropertyColumn14.Property = C1.Win.C1GanttView.TaskProperty.Notes;
            taskPropertyColumn14.Visible = false;
            this.c1GanttView1.Columns.Add(taskPropertyColumn1);
            this.c1GanttView1.Columns.Add(taskPropertyColumn2);
            this.c1GanttView1.Columns.Add(taskPropertyColumn3);
            this.c1GanttView1.Columns.Add(taskPropertyColumn4);
            this.c1GanttView1.Columns.Add(taskPropertyColumn5);
            this.c1GanttView1.Columns.Add(taskPropertyColumn6);
            this.c1GanttView1.Columns.Add(taskPropertyColumn7);
            this.c1GanttView1.Columns.Add(taskPropertyColumn8);
            this.c1GanttView1.Columns.Add(taskPropertyColumn9);
            this.c1GanttView1.Columns.Add(taskPropertyColumn10);
            this.c1GanttView1.Columns.Add(taskPropertyColumn11);
            this.c1GanttView1.Columns.Add(taskPropertyColumn12);
            this.c1GanttView1.Columns.Add(taskPropertyColumn13);
            this.c1GanttView1.Columns.Add(taskPropertyColumn14);
            this.c1GanttView1.DefaultWorkingTimes.Interval_1.Empty = false;
            this.c1GanttView1.DefaultWorkingTimes.Interval_1.From = new System.DateTime(1, 1, 1, 9, 0, 0, 0);
            this.c1GanttView1.DefaultWorkingTimes.Interval_1.To = new System.DateTime(1, 1, 1, 13, 0, 0, 0);
            this.c1GanttView1.DefaultWorkingTimes.Interval_2.Empty = false;
            this.c1GanttView1.DefaultWorkingTimes.Interval_2.From = new System.DateTime(1, 1, 1, 14, 0, 0, 0);
            this.c1GanttView1.DefaultWorkingTimes.Interval_2.To = new System.DateTime(1, 1, 1, 18, 0, 0, 0);
            this.c1GanttView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1GanttView1.Location = new System.Drawing.Point(0, 0);
            this.c1GanttView1.Name = "c1GanttView1";
            task1.ID = 993373120;
            task1.Mode = C1.Win.C1GanttView.TaskMode.Automatic;
            task1.Name = "项目摘要任务";
            this.c1GanttView1.ProjectSummary = task1;
            this.c1GanttView1.Size = new System.Drawing.Size(666, 403);
            this.c1GanttView1.StartDate = new System.DateTime(2014, 5, 7, 0, 0, 0, 0);
            this.c1GanttView1.TabIndex = 0;
            task2.ID = 634394271;
            task3.ID = 1486643194;
            task4.ID = 265352485;
            task5.ID = 1842803227;
            task6.ID = 992429958;
            task7.ID = 387159885;
            task8.ID = 1374087294;
            task9.ID = 390201858;
            task10.ID = 998006206;
            task11.ID = 1296148530;
            task12.ID = 318230682;
            task13.ID = 2021354355;
            task14.ID = 370973390;
            task15.ID = 957677849;
            task16.ID = 1407080033;
            task17.ID = 1831183098;
            task18.ID = 2102593908;
            task19.ID = 1236705894;
            task20.ID = 1908029356;
            task21.ID = 787582992;
            task22.ID = 1192970872;
            task23.ID = 235762917;
            task24.ID = 709241105;
            task25.ID = 427617794;
            task26.ID = 2042256738;
            task27.ID = 925128685;
            task28.ID = 386572465;
            task29.ID = 772155702;
            task30.ID = 14836556;
            task31.ID = 528646990;
            task32.ID = 2131605527;
            task33.ID = 17302759;
            task34.ID = 1094111484;
            task35.ID = 32282696;
            task36.ID = 858597519;
            task37.ID = 205970319;
            task38.ID = 642736461;
            task39.ID = 420870564;
            task40.ID = 1890838906;
            task41.ID = 608261624;
            task42.ID = 1675646304;
            task43.ID = 1956549165;
            task44.ID = 1996231248;
            task45.ID = 1897516081;
            task46.ID = 694449084;
            task47.ID = 713052691;
            task48.ID = 607882972;
            task49.ID = 2024766835;
            task50.ID = 1070691261;
            task51.ID = 2097736113;
            task52.ID = 257590733;
            task53.ID = 1577406127;
            task54.ID = 1579044014;
            task55.ID = 1792882758;
            task56.ID = 1947885896;
            task57.ID = 398631355;
            task58.ID = 777402090;
            task59.ID = 1985218339;
            task60.ID = 1948030137;
            task61.ID = 67301274;
            this.c1GanttView1.Tasks.Add(task2);
            this.c1GanttView1.Tasks.Add(task3);
            this.c1GanttView1.Tasks.Add(task4);
            this.c1GanttView1.Tasks.Add(task5);
            this.c1GanttView1.Tasks.Add(task6);
            this.c1GanttView1.Tasks.Add(task7);
            this.c1GanttView1.Tasks.Add(task8);
            this.c1GanttView1.Tasks.Add(task9);
            this.c1GanttView1.Tasks.Add(task10);
            this.c1GanttView1.Tasks.Add(task11);
            this.c1GanttView1.Tasks.Add(task12);
            this.c1GanttView1.Tasks.Add(task13);
            this.c1GanttView1.Tasks.Add(task14);
            this.c1GanttView1.Tasks.Add(task15);
            this.c1GanttView1.Tasks.Add(task16);
            this.c1GanttView1.Tasks.Add(task17);
            this.c1GanttView1.Tasks.Add(task18);
            this.c1GanttView1.Tasks.Add(task19);
            this.c1GanttView1.Tasks.Add(task20);
            this.c1GanttView1.Tasks.Add(task21);
            this.c1GanttView1.Tasks.Add(task22);
            this.c1GanttView1.Tasks.Add(task23);
            this.c1GanttView1.Tasks.Add(task24);
            this.c1GanttView1.Tasks.Add(task25);
            this.c1GanttView1.Tasks.Add(task26);
            this.c1GanttView1.Tasks.Add(task27);
            this.c1GanttView1.Tasks.Add(task28);
            this.c1GanttView1.Tasks.Add(task29);
            this.c1GanttView1.Tasks.Add(task30);
            this.c1GanttView1.Tasks.Add(task31);
            this.c1GanttView1.Tasks.Add(task32);
            this.c1GanttView1.Tasks.Add(task33);
            this.c1GanttView1.Tasks.Add(task34);
            this.c1GanttView1.Tasks.Add(task35);
            this.c1GanttView1.Tasks.Add(task36);
            this.c1GanttView1.Tasks.Add(task37);
            this.c1GanttView1.Tasks.Add(task38);
            this.c1GanttView1.Tasks.Add(task39);
            this.c1GanttView1.Tasks.Add(task40);
            this.c1GanttView1.Tasks.Add(task41);
            this.c1GanttView1.Tasks.Add(task42);
            this.c1GanttView1.Tasks.Add(task43);
            this.c1GanttView1.Tasks.Add(task44);
            this.c1GanttView1.Tasks.Add(task45);
            this.c1GanttView1.Tasks.Add(task46);
            this.c1GanttView1.Tasks.Add(task47);
            this.c1GanttView1.Tasks.Add(task48);
            this.c1GanttView1.Tasks.Add(task49);
            this.c1GanttView1.Tasks.Add(task50);
            this.c1GanttView1.Tasks.Add(task51);
            this.c1GanttView1.Tasks.Add(task52);
            this.c1GanttView1.Tasks.Add(task53);
            this.c1GanttView1.Tasks.Add(task54);
            this.c1GanttView1.Tasks.Add(task55);
            this.c1GanttView1.Tasks.Add(task56);
            this.c1GanttView1.Tasks.Add(task57);
            this.c1GanttView1.Tasks.Add(task58);
            this.c1GanttView1.Tasks.Add(task59);
            this.c1GanttView1.Tasks.Add(task60);
            this.c1GanttView1.Tasks.Add(task61);
            this.c1GanttView1.Timescale.BottomTier.Align = C1.Win.C1GanttView.ScaleLabelAlignment.Center;
            this.c1GanttView1.Timescale.BottomTier.Format = "ww";
            this.c1GanttView1.Timescale.BottomTier.Visible = true;
            this.c1GanttView1.Timescale.MiddleTier.Format = "nnnn d\'日\'";
            this.c1GanttView1.Timescale.MiddleTier.Units = C1.Win.C1GanttView.TimescaleUnits.Weeks;
            this.c1GanttView1.Timescale.MiddleTier.Visible = true;
            // 
            // GanttViewDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 403);
            this.Controls.Add(this.c1GanttView1);
            this.Name = "GanttViewDemo";
            this.Text = "GanttViewDemo";
            this.Load += new System.EventHandler(this.GanttViewDemo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c1GanttView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1GanttView.C1GanttView c1GanttView1;


    }
}