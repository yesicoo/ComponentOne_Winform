﻿namespace ControlExplorer.MenusToolbars
{
    partial class C1OutBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(C1OutBar));
            C1.C1Schedule.Printing.PrintStyle printStyle1 = new C1.C1Schedule.Printing.PrintStyle();
            C1.C1Schedule.Printing.PrintStyle printStyle2 = new C1.C1Schedule.Printing.PrintStyle();
            C1.C1Schedule.Printing.PrintStyle printStyle3 = new C1.C1Schedule.Printing.PrintStyle();
            C1.C1Schedule.Printing.PrintStyle printStyle4 = new C1.C1Schedule.Printing.PrintStyle();
            C1.C1Schedule.Printing.PrintStyle printStyle5 = new C1.C1Schedule.Printing.PrintStyle();
            this.c1OutBar1 = new C1.Win.C1Command.C1OutBar();
            this.c1OutPage1 = new C1.Win.C1Command.C1OutPage();
            this.c1ToolBar1 = new C1.Win.C1Command.C1ToolBar();
            this.c1CommandHolder1 = new C1.Win.C1Command.C1CommandHolder();
            this.c1Command1 = new C1.Win.C1Command.C1Command();
            this.c1Command2 = new C1.Win.C1Command.C1Command();
            this.c1Command3 = new C1.Win.C1Command.C1Command();
            this.c1Command4 = new C1.Win.C1Command.C1Command();
            this.c1Command5 = new C1.Win.C1Command.C1Command();
            this.c1Command6 = new C1.Win.C1Command.C1Command();
            this.c1Command7 = new C1.Win.C1Command.C1Command();
            this.c1Command8 = new C1.Win.C1Command.C1Command();
            this.c1Command9 = new C1.Win.C1Command.C1Command();
            this.c1Command10 = new C1.Win.C1Command.C1Command();
            this.c1Command11 = new C1.Win.C1Command.C1Command();
            this.c1CommandLink1 = new C1.Win.C1Command.C1CommandLink();
            this.c1CommandLink2 = new C1.Win.C1Command.C1CommandLink();
            this.c1CommandLink3 = new C1.Win.C1Command.C1CommandLink();
            this.c1CommandLink4 = new C1.Win.C1Command.C1CommandLink();
            this.c1CommandLink5 = new C1.Win.C1Command.C1CommandLink();
            this.c1CommandLink6 = new C1.Win.C1Command.C1CommandLink();
            this.c1OutPage3 = new C1.Win.C1Command.C1OutPage();
            this.c1Schedule1 = new C1.Win.C1Schedule.C1Schedule();
            this.c1CommandDock1 = new C1.Win.C1Command.C1CommandDock();
            this.c1DockingTab1 = new C1.Win.C1Command.C1DockingTab();
            this.c1DockingTabPage1 = new C1.Win.C1Command.C1DockingTabPage();
            this.c1Calendar1 = new C1.Win.C1Schedule.C1Calendar();
            this.c1OutPage2 = new C1.Win.C1Command.C1OutPage();
            this.c1ToolBar2 = new C1.Win.C1Command.C1ToolBar();
            this.c1CommandLink7 = new C1.Win.C1Command.C1CommandLink();
            this.c1CommandLink8 = new C1.Win.C1Command.C1CommandLink();
            this.c1CommandLink9 = new C1.Win.C1Command.C1CommandLink();
            this.c1CommandLink10 = new C1.Win.C1Command.C1CommandLink();
            this.c1CommandLink11 = new C1.Win.C1Command.C1CommandLink();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            ((System.ComponentModel.ISupportInitialize)(this.c1OutBar1)).BeginInit();
            this.c1OutBar1.SuspendLayout();
            this.c1OutPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1CommandHolder1)).BeginInit();
            this.c1OutPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1Schedule1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1CommandDock1)).BeginInit();
            this.c1CommandDock1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1DockingTab1)).BeginInit();
            this.c1DockingTab1.SuspendLayout();
            this.c1DockingTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1Calendar1)).BeginInit();
            this.c1OutPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1OutBar1
            // 
            this.c1OutBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.c1OutBar1.Controls.Add(this.c1OutPage1);
            this.c1OutBar1.Controls.Add(this.c1OutPage3);
            this.c1OutBar1.Controls.Add(this.c1OutPage2);
            this.c1OutBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.c1OutBar1.ImageList = this.imageList1;
            this.c1OutBar1.Location = new System.Drawing.Point(0, 0);
            this.c1OutBar1.Name = "c1OutBar1";
            this.c1OutBar1.PageTitleHeight = 28;
            this.c1OutBar1.ShowToolTips = true;
            this.c1OutBar1.Size = new System.Drawing.Size(225, 412);
            this.c1ThemeController1.SetTheme(this.c1OutBar1, "(default)");
            this.c1OutBar1.VisualStyle = C1.Win.C1Command.VisualStyle.Office2007Blue;
            this.c1OutBar1.VisualStyleBase = C1.Win.C1Command.VisualStyle.Office2007Blue;
            // 
            // c1OutPage1
            // 
            this.c1OutPage1.Controls.Add(this.c1ToolBar1);
            this.c1OutPage1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.c1OutPage1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1OutPage1.ImageIndex = 0;
            this.c1OutPage1.Name = "c1OutPage1";
            this.c1OutPage1.Size = new System.Drawing.Size(225, 328);
            this.c1OutPage1.Text = "我的Outlook";
            this.c1ThemeController1.SetTheme(this.c1OutPage1, "(default)");
            // 
            // c1ToolBar1
            // 
            this.c1ToolBar1.AccessibleName = "Tool Bar";
            this.c1ToolBar1.AutoSize = false;
            this.c1ToolBar1.ButtonLookVert = ((C1.Win.C1Command.ButtonLookFlags)((C1.Win.C1Command.ButtonLookFlags.Text | C1.Win.C1Command.ButtonLookFlags.Image)));
            this.c1ToolBar1.CommandHolder = this.c1CommandHolder1;
            this.c1ToolBar1.CommandLinks.AddRange(new C1.Win.C1Command.C1CommandLink[] {
            this.c1CommandLink1,
            this.c1CommandLink2,
            this.c1CommandLink3,
            this.c1CommandLink4,
            this.c1CommandLink5,
            this.c1CommandLink6});
            this.c1ToolBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1ToolBar1.Horizontal = false;
            this.c1ToolBar1.Location = new System.Drawing.Point(0, 0);
            this.c1ToolBar1.Movable = false;
            this.c1ToolBar1.Name = "c1ToolBar1";
            this.c1ToolBar1.Size = new System.Drawing.Size(225, 328);
            this.c1ToolBar1.Text = "第一页";
            this.c1ThemeController1.SetTheme(this.c1ToolBar1, "(default)");
            this.c1ToolBar1.VisualStyle = C1.Win.C1Command.VisualStyle.Office2010Blue;
            this.c1ToolBar1.VisualStyleBase = C1.Win.C1Command.VisualStyle.Office2010Blue;
            // 
            // c1CommandHolder1
            // 
            this.c1CommandHolder1.Commands.Add(this.c1Command1);
            this.c1CommandHolder1.Commands.Add(this.c1Command2);
            this.c1CommandHolder1.Commands.Add(this.c1Command3);
            this.c1CommandHolder1.Commands.Add(this.c1Command4);
            this.c1CommandHolder1.Commands.Add(this.c1Command5);
            this.c1CommandHolder1.Commands.Add(this.c1Command6);
            this.c1CommandHolder1.Commands.Add(this.c1Command7);
            this.c1CommandHolder1.Commands.Add(this.c1Command8);
            this.c1CommandHolder1.Commands.Add(this.c1Command9);
            this.c1CommandHolder1.Commands.Add(this.c1Command10);
            this.c1CommandHolder1.Commands.Add(this.c1Command11);
            this.c1CommandHolder1.Owner = this;
            this.c1CommandHolder1.CommandClick += new C1.Win.C1Command.CommandClickEventHandler(this.c1CommandHolder1_CommandClick);
            // 
            // c1Command1
            // 
            this.c1Command1.Image = ((System.Drawing.Image)(resources.GetObject("c1Command1.Image")));
            this.c1Command1.Name = "c1Command1";
            this.c1Command1.ShortcutText = "";
            this.c1Command1.Text = "收件箱";
            // 
            // c1Command2
            // 
            this.c1Command2.Image = global::ControlExplorer.Properties.Resources.CalendarSchedule;
            this.c1Command2.Name = "c1Command2";
            this.c1Command2.ShortcutText = "";
            this.c1Command2.Text = "日历";
            // 
            // c1Command3
            // 
            this.c1Command3.Image = global::ControlExplorer.Properties.Resources.AddressBook;
            this.c1Command3.Name = "c1Command3";
            this.c1Command3.ShortcutText = "";
            this.c1Command3.Text = "联系人";
            // 
            // c1Command4
            // 
            this.c1Command4.Image = global::ControlExplorer.Properties.Resources.Options;
            this.c1Command4.Name = "c1Command4";
            this.c1Command4.ShortcutText = "";
            this.c1Command4.Text = "任务";
            // 
            // c1Command5
            // 
            this.c1Command5.Image = global::ControlExplorer.Properties.Resources.Report;
            this.c1Command5.Name = "c1Command5";
            this.c1Command5.ShortcutText = "";
            this.c1Command5.Text = "日志";
            // 
            // c1Command6
            // 
            this.c1Command6.Image = global::ControlExplorer.Properties.Resources.EditComment;
            this.c1Command6.Name = "c1Command6";
            this.c1Command6.ShortcutText = "";
            this.c1Command6.Text = "记录";
            // 
            // c1Command7
            // 
            this.c1Command7.Image = global::ControlExplorer.Properties.Resources.FolderOpen;
            this.c1Command7.Name = "c1Command7";
            this.c1Command7.ShortcutText = "";
            this.c1Command7.Text = "我的文档";
            // 
            // c1Command8
            // 
            this.c1Command8.Image = global::ControlExplorer.Properties.Resources.SendMail;
            this.c1Command8.Name = "c1Command8";
            this.c1Command8.ShortcutText = "";
            this.c1Command8.Text = "已发送邮件";
            // 
            // c1Command9
            // 
            this.c1Command9.Image = global::ControlExplorer.Properties.Resources.SearchWeb;
            this.c1Command9.Name = "c1Command9";
            this.c1Command9.ShortcutText = "";
            this.c1Command9.Text = "网页搜索";
            // 
            // c1Command10
            // 
            this.c1Command10.Image = global::ControlExplorer.Properties.Resources.DeleteComment;
            this.c1Command10.Name = "c1Command10";
            this.c1Command10.ShortcutText = "";
            this.c1Command10.Text = "已删除邮件";
            // 
            // c1Command11
            // 
            this.c1Command11.Name = "c1Command11";
            this.c1Command11.ShortcutText = "";
            this.c1Command11.Text = "新增快捷方式...";
            // 
            // c1CommandLink1
            // 
            this.c1CommandLink1.ButtonLook = ((C1.Win.C1Command.ButtonLookFlags)((C1.Win.C1Command.ButtonLookFlags.Text | C1.Win.C1Command.ButtonLookFlags.Image)));
            this.c1CommandLink1.Command = this.c1Command1;
            // 
            // c1CommandLink2
            // 
            this.c1CommandLink2.Command = this.c1Command2;
            this.c1CommandLink2.SortOrder = 1;
            // 
            // c1CommandLink3
            // 
            this.c1CommandLink3.Command = this.c1Command3;
            this.c1CommandLink3.SortOrder = 2;
            // 
            // c1CommandLink4
            // 
            this.c1CommandLink4.Command = this.c1Command4;
            this.c1CommandLink4.SortOrder = 3;
            // 
            // c1CommandLink5
            // 
            this.c1CommandLink5.Command = this.c1Command5;
            this.c1CommandLink5.SortOrder = 4;
            // 
            // c1CommandLink6
            // 
            this.c1CommandLink6.Command = this.c1Command6;
            this.c1CommandLink6.SortOrder = 5;
            // 
            // c1OutPage3
            // 
            this.c1OutPage3.Controls.Add(this.c1Schedule1);
            this.c1OutPage3.Controls.Add(this.c1CommandDock1);
            this.c1OutPage3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.c1OutPage3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1OutPage3.ImageIndex = 1;
            this.c1OutPage3.Name = "c1OutPage3";
            this.c1OutPage3.Size = new System.Drawing.Size(225, 328);
            this.c1OutPage3.Text = "约会";
            this.c1ThemeController1.SetTheme(this.c1OutPage3, "(default)");
            // 
            // c1Schedule1
            // 
            // 
            // 
            // 
            this.c1Schedule1.CalendarInfo.CultureInfo = new System.Globalization.CultureInfo("zh-Hans");
            this.c1Schedule1.CalendarInfo.DateFormatString = "M/d/yyyy";
            this.c1Schedule1.CalendarInfo.EndDayTime = System.TimeSpan.Parse("19:00:00");
            this.c1Schedule1.CalendarInfo.StartDayTime = System.TimeSpan.Parse("07:00:00");
            this.c1Schedule1.CalendarInfo.TimeScale = System.TimeSpan.Parse("00:30:00");
            this.c1Schedule1.CalendarInfo.WeekStart = System.DayOfWeek.Sunday;
            this.c1Schedule1.CalendarInfo.WorkDays.AddRange(new System.DayOfWeek[] {
            System.DayOfWeek.Monday,
            System.DayOfWeek.Tuesday,
            System.DayOfWeek.Wednesday,
            System.DayOfWeek.Thursday,
            System.DayOfWeek.Friday});
            this.c1Schedule1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1Schedule1.GroupPageSize = 2;
            this.c1Schedule1.Location = new System.Drawing.Point(0, 0);
            this.c1Schedule1.Name = "c1Schedule1";
            printStyle1.Description = "每日样式";
            printStyle1.PreviewImage = ((System.Drawing.Image)(resources.GetObject("printStyle1.PreviewImage")));
            printStyle1.StyleName = "Daily";
            printStyle1.StyleSource = "day.c1d";
            printStyle2.Description = "每周样式";
            printStyle2.PreviewImage = ((System.Drawing.Image)(resources.GetObject("printStyle2.PreviewImage")));
            printStyle2.StyleName = "Week";
            printStyle2.StyleSource = "week.c1d";
            printStyle3.Description = "每月样式";
            printStyle3.PreviewImage = ((System.Drawing.Image)(resources.GetObject("printStyle3.PreviewImage")));
            printStyle3.StyleName = "Month";
            printStyle3.StyleSource = "month.c1d";
            printStyle4.Description = "细节样式";
            printStyle4.PreviewImage = ((System.Drawing.Image)(resources.GetObject("printStyle4.PreviewImage")));
            printStyle4.StyleName = "Details";
            printStyle4.StyleSource = "details.c1d";
            printStyle5.Context = C1.C1Schedule.Printing.PrintContextType.Appointment;
            printStyle5.Description = "备注样式";
            printStyle5.PreviewImage = ((System.Drawing.Image)(resources.GetObject("printStyle5.PreviewImage")));
            printStyle5.StyleName = "Memo";
            printStyle5.StyleSource = "memo.c1d";
            this.c1Schedule1.PrintInfo.PrintStyles.AddRange(new C1.C1Schedule.Printing.PrintStyle[] {
            printStyle1,
            printStyle2,
            printStyle3,
            printStyle4,
            printStyle5});
            // 
            // 
            // 
            this.c1Schedule1.Settings.FirstVisibleTime = System.TimeSpan.Parse("07:00:00");
            this.c1Schedule1.ShowReminderForm = false;
            this.c1Schedule1.ShowTitle = false;
            this.c1Schedule1.Size = new System.Drawing.Size(225, 109);
            this.c1Schedule1.TabIndex = 1;
            this.c1ThemeController1.SetTheme(this.c1Schedule1, "(default)");
            this.c1Schedule1.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.Office2010Blue;
            // 
            // c1CommandDock1
            // 
            this.c1CommandDock1.BackColor = System.Drawing.Color.White;
            this.c1CommandDock1.Controls.Add(this.c1DockingTab1);
            this.c1CommandDock1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.c1CommandDock1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.c1CommandDock1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1CommandDock1.Id = 1;
            this.c1CommandDock1.Location = new System.Drawing.Point(0, 109);
            this.c1CommandDock1.Name = "c1CommandDock1";
            this.c1CommandDock1.Size = new System.Drawing.Size(225, 219);
            this.c1ThemeController1.SetTheme(this.c1CommandDock1, "(default)");
            // 
            // c1DockingTab1
            // 
            this.c1DockingTab1.AutoHiding = true;
            this.c1DockingTab1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c1DockingTab1.CanAutoHide = true;
            this.c1DockingTab1.Controls.Add(this.c1DockingTabPage1);
            this.c1DockingTab1.Location = new System.Drawing.Point(0, 0);
            this.c1DockingTab1.Name = "c1DockingTab1";
            this.c1DockingTab1.Size = new System.Drawing.Size(225, 219);
            this.c1DockingTab1.TabIndex = 0;
            this.c1DockingTab1.TabSizeMode = C1.Win.C1Command.TabSizeModeEnum.Fit;
            this.c1DockingTab1.TabsSpacing = 7;
            this.c1DockingTab1.TabStyle = C1.Win.C1Command.TabStyleEnum.Office2010;
            this.c1ThemeController1.SetTheme(this.c1DockingTab1, "(default)");
            this.c1DockingTab1.VisualStyle = C1.Win.C1Command.VisualStyle.Office2010Blue;
            this.c1DockingTab1.VisualStyleBase = C1.Win.C1Command.VisualStyle.Office2010Blue;
            // 
            // c1DockingTabPage1
            // 
            this.c1DockingTabPage1.Controls.Add(this.c1Calendar1);
            this.c1DockingTabPage1.Location = new System.Drawing.Point(0, 28);
            this.c1DockingTabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.c1DockingTabPage1.Name = "c1DockingTabPage1";
            this.c1DockingTabPage1.Size = new System.Drawing.Size(225, 191);
            this.c1DockingTabPage1.TabIndex = 0;
            this.c1DockingTabPage1.Text = "日历";
            // 
            // c1Calendar1
            // 
            this.c1Calendar1.BoldedDates = new System.DateTime[0];
            this.c1Calendar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1Calendar1.Location = new System.Drawing.Point(0, 0);
            this.c1Calendar1.Margin = new System.Windows.Forms.Padding(4);
            this.c1Calendar1.Name = "c1Calendar1";
            this.c1Calendar1.Schedule = this.c1Schedule1;
            this.c1Calendar1.ShowWeekNumbers = false;
            this.c1Calendar1.Size = new System.Drawing.Size(225, 191);
            this.c1Calendar1.TabIndex = 0;
            this.c1ThemeController1.SetTheme(this.c1Calendar1, "(default)");
            this.c1Calendar1.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.Office2010Blue;
            // 
            // c1OutPage2
            // 
            this.c1OutPage2.Controls.Add(this.c1ToolBar2);
            this.c1OutPage2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.c1OutPage2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1OutPage2.ImageIndex = 2;
            this.c1OutPage2.Name = "c1OutPage2";
            this.c1OutPage2.Size = new System.Drawing.Size(225, 300);
            this.c1OutPage2.Text = "我的快捷方式";
            this.c1ThemeController1.SetTheme(this.c1OutPage2, "(default)");
            // 
            // c1ToolBar2
            // 
            this.c1ToolBar2.AccessibleName = "Tool Bar";
            this.c1ToolBar2.AutoSize = false;
            this.c1ToolBar2.ButtonAlign = System.Drawing.StringAlignment.Near;
            this.c1ToolBar2.ButtonLayoutVert = C1.Win.C1Command.ButtonLayoutEnum.TextOnRight;
            this.c1ToolBar2.ButtonLookVert = ((C1.Win.C1Command.ButtonLookFlags)((C1.Win.C1Command.ButtonLookFlags.Text | C1.Win.C1Command.ButtonLookFlags.Image)));
            this.c1ToolBar2.CommandHolder = this.c1CommandHolder1;
            this.c1ToolBar2.CommandLinks.AddRange(new C1.Win.C1Command.C1CommandLink[] {
            this.c1CommandLink7,
            this.c1CommandLink8,
            this.c1CommandLink9,
            this.c1CommandLink10,
            this.c1CommandLink11});
            this.c1ToolBar2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1ToolBar2.Horizontal = false;
            this.c1ToolBar2.Location = new System.Drawing.Point(0, 0);
            this.c1ToolBar2.Movable = false;
            this.c1ToolBar2.Name = "c1ToolBar2";
            this.c1ToolBar2.Size = new System.Drawing.Size(225, 300);
            this.c1ToolBar2.Text = "第二页";
            this.c1ThemeController1.SetTheme(this.c1ToolBar2, "(default)");
            this.c1ToolBar2.VisualStyle = C1.Win.C1Command.VisualStyle.Office2010Blue;
            this.c1ToolBar2.VisualStyleBase = C1.Win.C1Command.VisualStyle.Office2010Blue;
            // 
            // c1CommandLink7
            // 
            this.c1CommandLink7.ButtonLook = ((C1.Win.C1Command.ButtonLookFlags)((C1.Win.C1Command.ButtonLookFlags.Text | C1.Win.C1Command.ButtonLookFlags.Image)));
            this.c1CommandLink7.Command = this.c1Command7;
            // 
            // c1CommandLink8
            // 
            this.c1CommandLink8.Command = this.c1Command8;
            this.c1CommandLink8.SortOrder = 1;
            // 
            // c1CommandLink9
            // 
            this.c1CommandLink9.Command = this.c1Command9;
            this.c1CommandLink9.SortOrder = 2;
            // 
            // c1CommandLink10
            // 
            this.c1CommandLink10.Command = this.c1Command10;
            this.c1CommandLink10.SortOrder = 3;
            // 
            // c1CommandLink11
            // 
            this.c1CommandLink11.ButtonLook = C1.Win.C1Command.ButtonLookFlags.Text;
            this.c1CommandLink11.Command = this.c1Command11;
            this.c1CommandLink11.Delimiter = true;
            this.c1CommandLink11.SortOrder = 4;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Send.png");
            this.imageList1.Images.SetKeyName(1, "DateAndTime.png");
            this.imageList1.Images.SetKeyName(2, "Close1.png");
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.Control;
            this.splitter1.Location = new System.Drawing.Point(225, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 412);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            this.c1ThemeController1.SetTheme(this.splitter1, "(default)");
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.groupBox1.Location = new System.Drawing.Point(228, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(364, 412);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "收件箱";
            this.c1ThemeController1.SetTheme(this.groupBox1, "(default)");
            // 
            // C1OutBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 412);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.c1OutBar1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "C1OutBar";
            this.Text = "C1OutBar";
            this.c1ThemeController1.SetTheme(this, "(default)");
            this.Load += new System.EventHandler(this.C1OutBar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c1OutBar1)).EndInit();
            this.c1OutBar1.ResumeLayout(false);
            this.c1OutPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1CommandHolder1)).EndInit();
            this.c1OutPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1Schedule1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1CommandDock1)).EndInit();
            this.c1CommandDock1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1DockingTab1)).EndInit();
            this.c1DockingTab1.ResumeLayout(false);
            this.c1DockingTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1Calendar1)).EndInit();
            this.c1OutPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1Command.C1OutBar c1OutBar1;
        private C1.Win.C1Command.C1OutPage c1OutPage1;
        private C1.Win.C1Command.C1ToolBar c1ToolBar1;
        private C1.Win.C1Command.C1CommandHolder c1CommandHolder1;
        private System.Windows.Forms.Splitter splitter1;
        private C1.Win.C1Command.C1CommandLink c1CommandLink1;
        private C1.Win.C1Command.C1Command c1Command1;
        private C1.Win.C1Command.C1Command c1Command2;
        private C1.Win.C1Command.C1Command c1Command3;
        private C1.Win.C1Command.C1Command c1Command4;
        private C1.Win.C1Command.C1Command c1Command5;
        private C1.Win.C1Command.C1Command c1Command6;
        private C1.Win.C1Command.C1CommandLink c1CommandLink2;
        private C1.Win.C1Command.C1CommandLink c1CommandLink3;
        private C1.Win.C1Command.C1CommandLink c1CommandLink4;
        private C1.Win.C1Command.C1CommandLink c1CommandLink5;
        private C1.Win.C1Command.C1CommandLink c1CommandLink6;
        private C1.Win.C1Command.C1OutPage c1OutPage2;
        private C1.Win.C1Command.C1ToolBar c1ToolBar2;
        private System.Windows.Forms.GroupBox groupBox1;
        private C1.Win.C1Command.C1Command c1Command7;
        private C1.Win.C1Command.C1CommandLink c1CommandLink7;
        private C1.Win.C1Command.C1Command c1Command8;
        private C1.Win.C1Command.C1Command c1Command9;
        private C1.Win.C1Command.C1Command c1Command10;
        private C1.Win.C1Command.C1CommandLink c1CommandLink8;
        private C1.Win.C1Command.C1CommandLink c1CommandLink9;
        private C1.Win.C1Command.C1CommandLink c1CommandLink10;
        private C1.Win.C1Command.C1OutPage c1OutPage3;
        private C1.Win.C1Schedule.C1Schedule c1Schedule1;
        private C1.Win.C1Command.C1CommandDock c1CommandDock1;
        private C1.Win.C1Command.C1DockingTab c1DockingTab1;
        private C1.Win.C1Command.C1DockingTabPage c1DockingTabPage1;
        private C1.Win.C1Schedule.C1Calendar c1Calendar1;
        private C1.Win.C1Command.C1Command c1Command11;
        private C1.Win.C1Command.C1CommandLink c1CommandLink11;
        private System.Windows.Forms.ImageList imageList1;
        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;
    }
}