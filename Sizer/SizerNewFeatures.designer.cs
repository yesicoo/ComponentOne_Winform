namespace ControlExplorer.Sizer
{
    partial class SizerNewFeatures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Drawing.Drawing2D.Blend blend5 = new System.Drawing.Drawing2D.Blend();
            System.Drawing.Drawing2D.Blend blend1 = new System.Drawing.Drawing2D.Blend();
            System.Drawing.Drawing2D.Blend blend2 = new System.Drawing.Drawing2D.Blend();
            System.Drawing.Drawing2D.Blend blend3 = new System.Drawing.Drawing2D.Blend();
            System.Drawing.Drawing2D.Blend blend4 = new System.Drawing.Drawing2D.Blend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SizerNewFeatures));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.c1Sizer1 = new C1.Win.C1Sizer.C1Sizer();
            this.c1Sizer9 = new C1.Win.C1Sizer.C1Sizer();
            this.c1Sizer4 = new C1.Win.C1Sizer.C1Sizer();
            this.button13 = new C1.Win.C1Input.C1Button();
            this.button12 = new C1.Win.C1Input.C1Button();
            this.button11 = new C1.Win.C1Input.C1Button();
            this.button10 = new C1.Win.C1Input.C1Button();
            this.button9 = new C1.Win.C1Input.C1Button();
            this.button8 = new C1.Win.C1Input.C1Button();
            this.c1Sizer6 = new C1.Win.C1Sizer.C1Sizer();
            this.button7 = new C1.Win.C1Input.C1Button();
            this.button6 = new C1.Win.C1Input.C1Button();
            this.button5 = new C1.Win.C1Input.C1Button();
            this.button4 = new C1.Win.C1Input.C1Button();
            this.c1Sizer2 = new C1.Win.C1Sizer.C1Sizer();
            this.c1Sizer3 = new C1.Win.C1Sizer.C1Sizer();
            this.button25 = new C1.Win.C1Input.C1Button();
            this.button24 = new C1.Win.C1Input.C1Button();
            this.button23 = new C1.Win.C1Input.C1Button();
            this.button22 = new C1.Win.C1Input.C1Button();
            this.button3 = new C1.Win.C1Input.C1Button();
            this.button2 = new C1.Win.C1Input.C1Button();
            this.c1Sizer5 = new C1.Win.C1Sizer.C1Sizer();
            this.button21 = new C1.Win.C1Input.C1Button();
            this.button20 = new C1.Win.C1Input.C1Button();
            this.button19 = new C1.Win.C1Input.C1Button();
            this.button18 = new C1.Win.C1Input.C1Button();
            this.button17 = new C1.Win.C1Input.C1Button();
            this.button16 = new C1.Win.C1Input.C1Button();
            this.button15 = new C1.Win.C1Input.C1Button();
            this.button14 = new C1.Win.C1Input.C1Button();
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer1)).BeginInit();
            this.c1Sizer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer9)).BeginInit();
            this.c1Sizer9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer4)).BeginInit();
            this.c1Sizer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer6)).BeginInit();
            this.c1Sizer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer2)).BeginInit();
            this.c1Sizer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer3)).BeginInit();
            this.c1Sizer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer5)).BeginInit();
            this.c1Sizer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // c1Sizer1
            // 
            this.c1Sizer1.Controls.Add(this.c1Sizer9);
            this.c1Sizer1.Controls.Add(this.c1Sizer2);
            this.c1Sizer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1Sizer1.Gradient.BackColor2 = System.Drawing.Color.Yellow;
            blend5.Factors = new float[] {
        0F,
        1F};
            blend5.Positions = new float[] {
        0F,
        1F};
            this.c1Sizer1.Gradient.Blend = blend5;
            this.c1Sizer1.GridDefinition = "59.9502487562189:True:False;39.0547263681592:False:False;\t100:False:False;";
            this.c1Sizer1.Location = new System.Drawing.Point(0, 0);
            this.c1Sizer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Sizer1.Name = "c1Sizer1";
            this.c1Sizer1.Padding = new System.Windows.Forms.Padding(0);
            this.c1Sizer1.Size = new System.Drawing.Size(598, 402);
            this.c1Sizer1.TabIndex = 2;
            this.c1Sizer1.Text = "c1Sizer1";
            this.c1Sizer1.SplitterMoved += new C1.Win.C1Sizer.C1SizerEventHandler(this.c1Sizer1_SplitterMoved);
            // 
            // c1Sizer9
            // 
            this.c1Sizer9.Controls.Add(this.c1Sizer4);
            this.c1Sizer9.Controls.Add(this.c1Sizer6);
            this.c1Sizer9.GridDefinition = "100:False:False;\t64.0468227424749:True:False;35.2842809364548:False:False;";
            this.c1Sizer9.Location = new System.Drawing.Point(0, 245);
            this.c1Sizer9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Sizer9.Name = "c1Sizer9";
            this.c1Sizer9.Padding = new System.Windows.Forms.Padding(0);
            this.c1Sizer9.Size = new System.Drawing.Size(598, 157);
            this.c1Sizer9.TabIndex = 6;
            this.c1Sizer9.Text = "c1Sizer9";
            this.c1Sizer9.SplitterMoved += new C1.Win.C1Sizer.C1SizerEventHandler(this.c1Sizer9_SplitterMoved);
            // 
            // c1Sizer4
            // 
            this.c1Sizer4.BackColor = System.Drawing.Color.Black;
            this.c1Sizer4.Border.Color = System.Drawing.Color.SteelBlue;
            this.c1Sizer4.Border.Corners = new C1.Win.C1Sizer.Corners(20, 20, 0, 0);
            this.c1Sizer4.Border.Thickness = new System.Windows.Forms.Padding(5);
            this.c1Sizer4.Controls.Add(this.button13);
            this.c1Sizer4.Controls.Add(this.button12);
            this.c1Sizer4.Controls.Add(this.button11);
            this.c1Sizer4.Controls.Add(this.button10);
            this.c1Sizer4.Controls.Add(this.button9);
            this.c1Sizer4.Controls.Add(this.button8);
            this.c1Sizer4.Gradient.BackColor2 = System.Drawing.Color.LimeGreen;
            blend1.Factors = new float[] {
        0F,
        1F};
            blend1.Positions = new float[] {
        0F,
        1F};
            this.c1Sizer4.Gradient.Blend = blend1;
            this.c1Sizer4.Gradient.Mode = C1.Win.C1Sizer.GradientMode.Radial;
            this.c1Sizer4.GridDefinition = "11.4649681528662:False:False;29.2993630573248:False:False;15.2866242038217:False:" +
    "True;17.1974522292994:False:True;\t28.1984334203655:False:True;19.8433420365535:F" +
    "alse:False;42.5587467362924:False:True;";
            this.c1Sizer4.Location = new System.Drawing.Point(0, 0);
            this.c1Sizer4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Sizer4.Name = "c1Sizer4";
            this.c1Sizer4.Padding = new System.Windows.Forms.Padding(9, 10, 9, 10);
            this.c1Sizer4.Size = new System.Drawing.Size(383, 157);
            this.c1Sizer4.TabIndex = 2;
            this.c1Sizer4.Text = "c1Sizer4";
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button13.Location = new System.Drawing.Point(14, 15);
            this.button13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(108, 18);
            this.button13.TabIndex = 5;
            this.button13.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button13, "(default)");
            this.button13.UseVisualStyleBackColor = true;
            this.button13.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button12.Location = new System.Drawing.Point(14, 37);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(355, 74);
            this.button12.TabIndex = 4;
            this.button12.Text = "����";
            this.c1ThemeController1.SetTheme(this.button12, "(default)");
            this.button12.UseVisualStyleBackColor = true;
            this.button12.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button11.Location = new System.Drawing.Point(14, 115);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(188, 27);
            this.button11.TabIndex = 3;
            this.button11.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button11, "(default)");
            this.button11.UseVisualStyleBackColor = true;
            this.button11.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button10.Location = new System.Drawing.Point(126, 15);
            this.button10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(243, 18);
            this.button10.TabIndex = 2;
            this.button10.Text = "����";
            this.c1ThemeController1.SetTheme(this.button10, "(default)");
            this.button10.UseVisualStyleBackColor = true;
            this.button10.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button9.Location = new System.Drawing.Point(206, 115);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(163, 27);
            this.button9.TabIndex = 1;
            this.button9.Text = "�̶��ߺͿ�";
            this.c1ThemeController1.SetTheme(this.button9, "(default)");
            this.button9.UseVisualStyleBackColor = true;
            this.button9.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button8.Location = new System.Drawing.Point(206, 115);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(163, 27);
            this.button8.TabIndex = 0;
            this.button8.Text = "�̶��ߺͿ�";
            this.c1ThemeController1.SetTheme(this.button8, "(default)");
            this.button8.UseVisualStyleBackColor = true;
            this.button8.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // c1Sizer6
            // 
            this.c1Sizer6.BackColor = System.Drawing.Color.LavenderBlush;
            this.c1Sizer6.Border.Color = System.Drawing.Color.SteelBlue;
            this.c1Sizer6.Border.Corners = new C1.Win.C1Sizer.Corners(20, 20, 0, 0);
            this.c1Sizer6.Border.Thickness = new System.Windows.Forms.Padding(5);
            this.c1Sizer6.Controls.Add(this.button7);
            this.c1Sizer6.Controls.Add(this.button6);
            this.c1Sizer6.Controls.Add(this.button5);
            this.c1Sizer6.Controls.Add(this.button4);
            this.c1Sizer6.Gradient.BackColor2 = System.Drawing.Color.Orchid;
            blend2.Factors = new float[] {
        1F,
        0.4345982F,
        0.1888756F,
        0.082085F,
        0.03567399F,
        0.01550385F,
        0.006737947F};
            blend2.Positions = new float[] {
        0F,
        0.1666667F,
        0.3333333F,
        0.5F,
        0.6666667F,
        0.8333333F,
        1F};
            this.c1Sizer6.Gradient.Blend = blend2;
            this.c1Sizer6.Gradient.Center = new System.Drawing.Point(51, 55);
            this.c1Sizer6.Gradient.Mode = C1.Win.C1Sizer.GradientMode.Radial;
            this.c1Sizer6.GridDefinition = "13.3757961783439:False:True;38.2165605095541:False:False;24.203821656051:False:Tr" +
    "ue;\t49.2890995260664:False:True;35.5450236966825:False:False;";
            this.c1Sizer6.Location = new System.Drawing.Point(387, 0);
            this.c1Sizer6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Sizer6.Name = "c1Sizer6";
            this.c1Sizer6.Padding = new System.Windows.Forms.Padding(9, 10, 9, 10);
            this.c1Sizer6.Size = new System.Drawing.Size(211, 157);
            this.c1Sizer6.TabIndex = 4;
            this.c1Sizer6.Text = "c1Sizer6";
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button7.Location = new System.Drawing.Point(14, 15);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(183, 21);
            this.button7.TabIndex = 3;
            this.button7.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button7, "(default)");
            this.button7.UseVisualStyleBackColor = true;
            this.button7.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button6.Location = new System.Drawing.Point(122, 40);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 60);
            this.button6.TabIndex = 2;
            this.button6.Text = "����";
            this.c1ThemeController1.SetTheme(this.button6, "(default)");
            this.button6.UseVisualStyleBackColor = true;
            this.button6.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button5.Location = new System.Drawing.Point(122, 104);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 38);
            this.button5.TabIndex = 1;
            this.button5.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button5, "(default)");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button4.Location = new System.Drawing.Point(14, 40);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(104, 102);
            this.button4.TabIndex = 0;
            this.button4.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button4, "(default)");
            this.button4.UseVisualStyleBackColor = true;
            this.button4.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // c1Sizer2
            // 
            this.c1Sizer2.Controls.Add(this.c1Sizer3);
            this.c1Sizer2.Controls.Add(this.c1Sizer5);
            this.c1Sizer2.GridDefinition = "100:False:False;\t28.9297658862876:True:False;70.4013377926421:False:False;";
            this.c1Sizer2.Location = new System.Drawing.Point(0, 0);
            this.c1Sizer2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Sizer2.Name = "c1Sizer2";
            this.c1Sizer2.Padding = new System.Windows.Forms.Padding(0);
            this.c1Sizer2.Size = new System.Drawing.Size(598, 241);
            this.c1Sizer2.TabIndex = 5;
            this.c1Sizer2.Text = "c1Sizer2";
            this.c1Sizer2.SplitterMoved += new C1.Win.C1Sizer.C1SizerEventHandler(this.c1Sizer2_SplitterMoved);
            // 
            // c1Sizer3
            // 
            this.c1Sizer3.BackColor = System.Drawing.Color.OrangeRed;
            this.c1Sizer3.Border.Color = System.Drawing.Color.SteelBlue;
            this.c1Sizer3.Border.Corners = new C1.Win.C1Sizer.Corners(10, 10, 10, 10);
            this.c1Sizer3.Border.Thickness = new System.Windows.Forms.Padding(5);
            this.c1Sizer3.Controls.Add(this.button25);
            this.c1Sizer3.Controls.Add(this.button24);
            this.c1Sizer3.Controls.Add(this.button23);
            this.c1Sizer3.Controls.Add(this.button22);
            this.c1Sizer3.Controls.Add(this.button3);
            this.c1Sizer3.Controls.Add(this.button2);
            this.c1Sizer3.Gradient.BackColor2 = System.Drawing.Color.Yellow;
            blend3.Factors = new float[] {
        0F,
        1F};
            blend3.Positions = new float[] {
        0F,
        1F};
            this.c1Sizer3.Gradient.Blend = blend3;
            this.c1Sizer3.Gradient.Mode = C1.Win.C1Sizer.GradientMode.DiagonalUp;
            this.c1Sizer3.GridDefinition = "43.9834024896266:False:True;3.7344398340249:False:False;9.12863070539419:False:Tr" +
    "ue;14.9377593360996:False:True;9.12863070539419:False:False;\t35.2601156069364:Fa" +
    "lse:False;46.242774566474:False:True;";
            this.c1Sizer3.Location = new System.Drawing.Point(0, 0);
            this.c1Sizer3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Sizer3.Name = "c1Sizer3";
            this.c1Sizer3.Padding = new System.Windows.Forms.Padding(9, 10, 9, 10);
            this.c1Sizer3.Size = new System.Drawing.Size(173, 241);
            this.c1Sizer3.TabIndex = 1;
            this.c1Sizer3.Text = "c1Sizer3";
            // 
            // button25
            // 
            this.button25.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button25.Location = new System.Drawing.Point(14, 125);
            this.button25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(61, 9);
            this.button25.TabIndex = 10;
            this.button25.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button25, "(default)");
            this.button25.UseVisualStyleBackColor = true;
            this.button25.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button24
            // 
            this.button24.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button24.Location = new System.Drawing.Point(79, 164);
            this.button24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(80, 36);
            this.button24.TabIndex = 9;
            this.button24.Text = "�̶��ߺͿ�";
            this.c1ThemeController1.SetTheme(this.button24, "(default)");
            this.button24.UseVisualStyleBackColor = true;
            this.button24.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button23
            // 
            this.button23.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button23.Location = new System.Drawing.Point(14, 204);
            this.button23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(145, 22);
            this.button23.TabIndex = 8;
            this.button23.Text = "����";
            this.c1ThemeController1.SetTheme(this.button23, "(default)");
            this.button23.UseVisualStyleBackColor = true;
            this.button23.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button22
            // 
            this.button22.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button22.Location = new System.Drawing.Point(14, 138);
            this.button22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(61, 62);
            this.button22.TabIndex = 7;
            this.button22.Text = "����";
            this.c1ThemeController1.SetTheme(this.button22, "(default)");
            this.button22.UseVisualStyleBackColor = true;
            this.button22.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button3.Location = new System.Drawing.Point(79, 15);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 145);
            this.button3.TabIndex = 6;
            this.button3.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button3, "(default)");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button2.Location = new System.Drawing.Point(14, 15);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(61, 106);
            this.button2.TabIndex = 5;
            this.button2.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button2, "(default)");
            this.button2.UseVisualStyleBackColor = true;
            this.button2.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // c1Sizer5
            // 
            this.c1Sizer5.BackColor = System.Drawing.Color.LightCyan;
            this.c1Sizer5.Border.Color = System.Drawing.Color.SteelBlue;
            this.c1Sizer5.Border.Corners = new C1.Win.C1Sizer.Corners(0, 30, 30, 0);
            this.c1Sizer5.Border.Thickness = new System.Windows.Forms.Padding(6);
            this.c1Sizer5.Controls.Add(this.button21);
            this.c1Sizer5.Controls.Add(this.button20);
            this.c1Sizer5.Controls.Add(this.button19);
            this.c1Sizer5.Controls.Add(this.button18);
            this.c1Sizer5.Controls.Add(this.button17);
            this.c1Sizer5.Controls.Add(this.button16);
            this.c1Sizer5.Controls.Add(this.button15);
            this.c1Sizer5.Controls.Add(this.button14);
            this.c1Sizer5.Gradient.BackColor2 = System.Drawing.Color.DeepSkyBlue;
            blend4.Factors = new float[] {
        0F,
        0.75F,
        0.25F,
        0F,
        0.59F,
        1F};
            blend4.Positions = new float[] {
        0F,
        0.25F,
        0.49F,
        0.51F,
        0.75F,
        1F};
            this.c1Sizer5.Gradient.Blend = blend4;
            this.c1Sizer5.Gradient.Mode = C1.Win.C1Sizer.GradientMode.DiagonalDown;
            this.c1Sizer5.GridDefinition = resources.GetString("c1Sizer5.GridDefinition");
            this.c1Sizer5.Location = new System.Drawing.Point(177, 0);
            this.c1Sizer5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Sizer5.Name = "c1Sizer5";
            this.c1Sizer5.Padding = new System.Windows.Forms.Padding(9, 10, 9, 10);
            this.c1Sizer5.Size = new System.Drawing.Size(421, 241);
            this.c1Sizer5.TabIndex = 3;
            this.c1Sizer5.Text = "c1Sizer5";
            // 
            // button21
            // 
            this.button21.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button21.Location = new System.Drawing.Point(333, 155);
            this.button21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(73, 26);
            this.button21.TabIndex = 7;
            this.button21.Text = "�̶��ߺͿ�";
            this.c1ThemeController1.SetTheme(this.button21, "(default)");
            this.button21.UseVisualStyleBackColor = true;
            this.button21.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button20
            // 
            this.button20.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button20.Location = new System.Drawing.Point(244, 155);
            this.button20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(85, 26);
            this.button20.TabIndex = 6;
            this.button20.Text = "�̶��ߺͿ�";
            this.c1ThemeController1.SetTheme(this.button20, "(default)");
            this.button20.UseVisualStyleBackColor = true;
            this.button20.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button19.Location = new System.Drawing.Point(97, 155);
            this.button19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(143, 26);
            this.button19.TabIndex = 5;
            this.button19.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button19, "(default)");
            this.button19.UseVisualStyleBackColor = true;
            this.button19.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button18
            // 
            this.button18.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button18.Location = new System.Drawing.Point(97, 185);
            this.button18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(309, 40);
            this.button18.TabIndex = 4;
            this.button18.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button18, "(default)");
            this.button18.UseVisualStyleBackColor = true;
            this.button18.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button17.Location = new System.Drawing.Point(97, 47);
            this.button17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(309, 104);
            this.button17.TabIndex = 3;
            this.button17.Text = "����";
            this.c1ThemeController1.SetTheme(this.button17, "(default)");
            this.button17.UseVisualStyleBackColor = true;
            this.button17.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button16.Location = new System.Drawing.Point(15, 185);
            this.button16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(78, 40);
            this.button16.TabIndex = 2;
            this.button16.Text = "�̶��ߺͿ�";
            this.c1ThemeController1.SetTheme(this.button16, "(default)");
            this.button16.UseVisualStyleBackColor = true;
            this.button16.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button15.Location = new System.Drawing.Point(15, 47);
            this.button15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(78, 178);
            this.button15.TabIndex = 1;
            this.button15.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button15, "(default)");
            this.button15.UseVisualStyleBackColor = true;
            this.button15.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.button14.Location = new System.Drawing.Point(15, 16);
            this.button14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(391, 27);
            this.button14.TabIndex = 0;
            this.button14.Text = "�̶���";
            this.c1ThemeController1.SetTheme(this.button14, "(default)");
            this.button14.UseVisualStyleBackColor = true;
            this.button14.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // SizerNewFeatures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.ClientSize = new System.Drawing.Size(598, 402);
            this.Controls.Add(this.c1Sizer1);
            this.Font = new System.Drawing.Font("΢���ź�", 9F);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "SizerNewFeatures";
            this.c1ThemeController1.SetTheme(this, "(default)");
            this.Load += new System.EventHandler(this.SizerNewFeatures_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer1)).EndInit();
            this.c1Sizer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer9)).EndInit();
            this.c1Sizer9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer4)).EndInit();
            this.c1Sizer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer6)).EndInit();
            this.c1Sizer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer2)).EndInit();
            this.c1Sizer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer3)).EndInit();
            this.c1Sizer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1Sizer5)).EndInit();
            this.c1Sizer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private C1.Win.C1Sizer.C1Sizer c1Sizer1;
        private C1.Win.C1Sizer.C1Sizer c1Sizer9;
        private C1.Win.C1Sizer.C1Sizer c1Sizer4;
        private C1.Win.C1Input.C1Button button13;
        private C1.Win.C1Input.C1Button button12;
        private C1.Win.C1Input.C1Button button11;
        private C1.Win.C1Input.C1Button button10;
        private C1.Win.C1Input.C1Button button9;
        private C1.Win.C1Input.C1Button button8;
        private C1.Win.C1Sizer.C1Sizer c1Sizer6;
        private C1.Win.C1Input.C1Button button7;
        private C1.Win.C1Input.C1Button button6;
        private C1.Win.C1Input.C1Button button5;
        private C1.Win.C1Input.C1Button button4;
        private C1.Win.C1Sizer.C1Sizer c1Sizer2;
        private C1.Win.C1Sizer.C1Sizer c1Sizer3;
        private C1.Win.C1Input.C1Button button25;
        private C1.Win.C1Input.C1Button button24;
        private C1.Win.C1Input.C1Button button23;
        private C1.Win.C1Input.C1Button button22;
        private C1.Win.C1Input.C1Button button3;
        private C1.Win.C1Input.C1Button button2;
        private C1.Win.C1Sizer.C1Sizer c1Sizer5;
        private C1.Win.C1Input.C1Button button21;
        private C1.Win.C1Input.C1Button button20;
        private C1.Win.C1Input.C1Button button19;
        private C1.Win.C1Input.C1Button button18;
        private C1.Win.C1Input.C1Button button17;
        private C1.Win.C1Input.C1Button button16;
        private C1.Win.C1Input.C1Button button15;
        private C1.Win.C1Input.C1Button button14;
        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;
    }
}
