﻿namespace ControlExplorer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            C1.Win.C1Tile.TextElement textElement2 = new C1.Win.C1Tile.TextElement();
            C1.Win.C1Tile.TextElement textElement3 = new C1.Win.C1Tile.TextElement();
            C1.Win.C1Tile.ImageElement imageElement1 = new C1.Win.C1Tile.ImageElement();
            C1.Win.C1Tile.TextElement textElement1 = new C1.Win.C1Tile.TextElement();
            C1.Win.C1Tile.ImageElement imageElement2 = new C1.Win.C1Tile.ImageElement();
            C1.Win.C1Tile.ImageElement imageElement3 = new C1.Win.C1Tile.ImageElement();
            C1.Win.C1Tile.TextElement textElement4 = new C1.Win.C1Tile.TextElement();
            C1.Win.C1Tile.TextElement textElement5 = new C1.Win.C1Tile.TextElement();
            C1.Win.C1Tile.TextElement textElement6 = new C1.Win.C1Tile.TextElement();
            C1.Win.C1Tile.TextElement textElement7 = new C1.Win.C1Tile.TextElement();
            C1.Win.C1Tile.TextElement textElement8 = new C1.Win.C1Tile.TextElement();
            C1.Win.C1Tile.ImageElement imageElement4 = new C1.Win.C1Tile.ImageElement();
            C1.Win.C1Tile.TextElement textElement9 = new C1.Win.C1Tile.TextElement();
            C1.Win.C1Tile.TextElement textElement10 = new C1.Win.C1Tile.TextElement();
            this.controlIcons = new System.Windows.Forms.ImageList(this.components);
            this.c1TaskbarButton1 = new C1.Win.C1Win7Pack.C1TaskbarButton(this.components);
            this.sampleIcons = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.c1StatusBar1 = new C1.Win.C1Ribbon.C1StatusBar();
            this.c1InputPanel1 = new C1.Win.C1InputPanel.C1InputPanel();
            this.vsLabel = new C1.Win.C1InputPanel.InputLabel();
            this.comboVisualStyles = new C1.Win.C1InputPanel.InputComboBox();
            this.office2007BlueOption = new C1.Win.C1InputPanel.InputOption();
            this.office2007SilverOption = new C1.Win.C1InputPanel.InputOption();
            this.office2007BlackOption = new C1.Win.C1InputPanel.InputOption();
            this.office2010BlueOption = new C1.Win.C1InputPanel.InputOption();
            this.office2010SilverOption = new C1.Win.C1InputPanel.InputOption();
            this.office2010BlackOption = new C1.Win.C1InputPanel.InputOption();
            this.windowsOption = new C1.Win.C1InputPanel.InputOption();
            this.stateImage = new System.Windows.Forms.ImageList(this.components);
            this.template1 = new C1.Win.C1Tile.Template();
            this.controls = new C1.Win.C1Tile.Group();
            this.c1TileControl1 = new C1.Win.C1Tile.C1TileControl();
            this.template2 = new C1.Win.C1Tile.Template();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this._backBtn = new System.Windows.Forms.Button();
            this.c1TileControl2 = new C1.Win.C1Tile.C1TileControl();
            this.group2 = new C1.Win.C1Tile.Group();
            this.tile2 = new C1.Win.C1Tile.Tile();
            this.template3 = new C1.Win.C1Tile.Template();
            this.template4 = new C1.Win.C1Tile.Template();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.c1StatusBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1InputPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // controlIcons
            // 
            this.controlIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("controlIcons.ImageStream")));
            this.controlIcons.TransparentColor = System.Drawing.Color.Red;
            this.controlIcons.Images.SetKeyName(0, "C1Themes.png");
            this.controlIcons.Images.SetKeyName(1, "TileControl_48px.png");
            this.controlIcons.Images.SetKeyName(2, "Barcode_48px.png");
            this.controlIcons.Images.SetKeyName(3, "Chart_48px.png");
            this.controlIcons.Images.SetKeyName(4, "DockingTab_48px.png");
            this.controlIcons.Images.SetKeyName(5, "DynamicHelp_48px.png");
            this.controlIcons.Images.SetKeyName(6, "Editor_48px.png");
            this.controlIcons.Images.SetKeyName(7, "Excel_48px.png");
            this.controlIcons.Images.SetKeyName(8, "FlexGrid_48px.png");
            this.controlIcons.Images.SetKeyName(9, "GanttView_48px.png");
            this.controlIcons.Images.SetKeyName(10, "Gauge_48px.png");
            this.controlIcons.Images.SetKeyName(11, "Input_48px.png");
            this.controlIcons.Images.SetKeyName(12, "InputPanel_48px.png");
            this.controlIcons.Images.SetKeyName(13, "List_48px.png");
            this.controlIcons.Images.SetKeyName(14, "Menu_48px.png");
            this.controlIcons.Images.SetKeyName(15, "PDF_48px.png");
            this.controlIcons.Images.SetKeyName(16, "Report_48px.png");
            this.controlIcons.Images.SetKeyName(17, "Ribbon_48px.png");
            this.controlIcons.Images.SetKeyName(18, "Scheduler_48px.png");
            this.controlIcons.Images.SetKeyName(19, "Sizer_48px.png");
            this.controlIcons.Images.SetKeyName(20, "SpellChecker_48px.png");
            this.controlIcons.Images.SetKeyName(21, "SplitContainer_48px.png");
            this.controlIcons.Images.SetKeyName(22, "superTooltip_48px.png");
            this.controlIcons.Images.SetKeyName(23, "TrueDBGrid_48px.png");
            this.controlIcons.Images.SetKeyName(24, "Win7Pack_48px.png");
            // 
            // c1TaskbarButton1
            // 
            this.c1TaskbarButton1.ContainerForm = this;
            this.c1TaskbarButton1.WindowAppID = "MainForm";
            // 
            // sampleIcons
            // 
            this.sampleIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("sampleIcons.ImageStream")));
            this.sampleIcons.TransparentColor = System.Drawing.Color.Red;
            this.sampleIcons.Images.SetKeyName(0, "Home.png");
            this.sampleIcons.Images.SetKeyName(1, "demo.gif");
            this.sampleIcons.Images.SetKeyName(2, "new2.gif");
            this.sampleIcons.Images.SetKeyName(3, "c1hexwinforms.png");
            this.sampleIcons.Images.SetKeyName(4, "HistoryBack.png");
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(353, 244);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(918, 658);
            this.panel1.TabIndex = 4;
            this.panel1.Visible = false;
            // 
            // c1StatusBar1
            // 
            this.c1StatusBar1.Location = new System.Drawing.Point(0, 903);
            this.c1StatusBar1.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.c1StatusBar1.Name = "c1StatusBar1";
            this.c1StatusBar1.Size = new System.Drawing.Size(1283, 23);
            // 
            // c1InputPanel1
            // 
            this.c1InputPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.c1InputPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.c1InputPanel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1InputPanel1.Items.Add(this.vsLabel);
            this.c1InputPanel1.Items.Add(this.comboVisualStyles);
            this.c1InputPanel1.Location = new System.Drawing.Point(1024, 0);
            this.c1InputPanel1.Margin = new System.Windows.Forms.Padding(1, 2, 1, 2);
            this.c1InputPanel1.Name = "c1InputPanel1";
            this.c1InputPanel1.Padding = new System.Windows.Forms.Padding(2, 16, 2, 2);
            this.c1InputPanel1.Size = new System.Drawing.Size(260, 111);
            this.c1InputPanel1.TabIndex = 7;
            this.c1InputPanel1.VisualStyle = C1.Win.C1InputPanel.VisualStyle.Windows7;
            // 
            // vsLabel
            // 
            this.vsLabel.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vsLabel.Name = "vsLabel";
            this.vsLabel.Text = "视觉风格:";
            // 
            // comboVisualStyles
            // 
            this.comboVisualStyles.DropDownStyle = C1.Win.C1InputPanel.InputComboBoxStyle.DropDownList;
            this.comboVisualStyles.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboVisualStyles.Items.Add(this.office2007BlueOption);
            this.comboVisualStyles.Items.Add(this.office2007SilverOption);
            this.comboVisualStyles.Items.Add(this.office2007BlackOption);
            this.comboVisualStyles.Items.Add(this.office2010BlueOption);
            this.comboVisualStyles.Items.Add(this.office2010SilverOption);
            this.comboVisualStyles.Items.Add(this.office2010BlackOption);
            this.comboVisualStyles.Items.Add(this.windowsOption);
            this.comboVisualStyles.Name = "comboVisualStyles";
            this.comboVisualStyles.Width = 138;
            this.comboVisualStyles.SelectedIndexChanged += new System.EventHandler(this.comboVisualStyles_SelectedIndexChanged);
            // 
            // office2007BlueOption
            // 
            this.office2007BlueOption.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.office2007BlueOption.Name = "office2007BlueOption";
            this.office2007BlueOption.Text = "Office 2007 Blue";
            // 
            // office2007SilverOption
            // 
            this.office2007SilverOption.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.office2007SilverOption.Name = "office2007SilverOption";
            this.office2007SilverOption.Text = "Office 2007 Silver";
            // 
            // office2007BlackOption
            // 
            this.office2007BlackOption.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.office2007BlackOption.Name = "office2007BlackOption";
            this.office2007BlackOption.Text = "Office 2007 Black";
            // 
            // office2010BlueOption
            // 
            this.office2010BlueOption.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.office2010BlueOption.Name = "office2010BlueOption";
            this.office2010BlueOption.Text = "Office 2010 Blue";
            // 
            // office2010SilverOption
            // 
            this.office2010SilverOption.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.office2010SilverOption.Name = "office2010SilverOption";
            this.office2010SilverOption.Text = "Office 2010 Silver";
            // 
            // office2010BlackOption
            // 
            this.office2010BlackOption.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.office2010BlackOption.Name = "office2010BlackOption";
            this.office2010BlackOption.Text = "Office 2010 Black";
            // 
            // windowsOption
            // 
            this.windowsOption.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.windowsOption.Name = "windowsOption";
            this.windowsOption.Text = "Windows";
            // 
            // stateImage
            // 
            this.stateImage.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("stateImage.ImageStream")));
            this.stateImage.TransparentColor = System.Drawing.Color.Red;
            this.stateImage.Images.SetKeyName(0, "Options.png");
            // 
            // template1
            // 
            this.template1.Description = "MainInfo";
            textElement2.Alignment = System.Drawing.ContentAlignment.TopLeft;
            textElement2.Font = new System.Drawing.Font("微软雅黑", 36F);
            textElement2.Margin = new System.Windows.Forms.Padding(18, 18, 0, 0);
            textElement3.Alignment = System.Drawing.ContentAlignment.TopLeft;
            textElement3.Font = new System.Drawing.Font("微软雅黑", 14F);
            textElement3.Margin = new System.Windows.Forms.Padding(18, 182, 6, 0);
            textElement3.TextSelector = C1.Win.C1Tile.TextSelector.Text1;
            this.template1.Elements.Add(textElement2);
            this.template1.Elements.Add(textElement3);
            this.template1.Name = "template1";
            // 
            // controls
            // 
            this.controls.Name = "controls";
            // 
            // c1TileControl1
            // 
            this.c1TileControl1.AllowChecking = true;
            this.c1TileControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.c1TileControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.c1TileControl1.CellHeight = 145;
            this.c1TileControl1.CellSpacing = 13;
            this.c1TileControl1.CellWidth = 145;
            // 
            // 
            // 
            imageElement1.SymbolSize = C1.Win.C1Tile.SymbolSize.Image48x48;
            textElement1.Alignment = System.Drawing.ContentAlignment.BottomCenter;
            textElement1.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            textElement1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 6);
            this.c1TileControl1.DefaultTemplate.Elements.Add(imageElement1);
            this.c1TileControl1.DefaultTemplate.Elements.Add(textElement1);
            this.c1TileControl1.GroupPadding = new System.Windows.Forms.Padding(13);
            this.c1TileControl1.Groups.Add(this.controls);
            this.c1TileControl1.GroupTextSize = 28F;
            this.c1TileControl1.GroupTextX = 24;
            this.c1TileControl1.GroupTextY = 6;
            this.c1TileControl1.ImageList = this.controlIcons;
            this.c1TileControl1.Location = new System.Drawing.Point(337, 192);
            this.c1TileControl1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.c1TileControl1.Name = "c1TileControl1";
            this.c1TileControl1.Padding = new System.Windows.Forms.Padding(0, 24, 0, 0);
            this.c1TileControl1.SBThumbBorderColor = System.Drawing.Color.White;
            this.c1TileControl1.SBThumbInnerColor = System.Drawing.Color.White;
            this.c1TileControl1.Size = new System.Drawing.Size(954, 693);
            this.c1TileControl1.SurfacePadding = new System.Windows.Forms.Padding(31, 6, 18, 6);
            this.c1TileControl1.SwipeDistance = 31;
            this.c1TileControl1.SwipeRearrangeDistance = 151;
            this.c1TileControl1.TabIndex = 8;
            this.c1TileControl1.Templates.Add(this.template1);
            this.c1TileControl1.Templates.Add(this.template2);
            this.c1TileControl1.Text = resources.GetString("c1TileControl1.Text");
            this.c1TileControl1.TextX = 42;
            this.c1TileControl1.TextY = 18;
            this.c1TileControl1.TileBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.c1TileControl1.TileClicked += new System.EventHandler<C1.Win.C1Tile.TileEventArgs>(this.c1TileControl1_TileClicked);
            // 
            // template2
            // 
            this.template2.Description = "NewItmes";
            imageElement2.SymbolSize = C1.Win.C1Tile.SymbolSize.Image48x48;
            imageElement3.Alignment = System.Drawing.ContentAlignment.TopLeft;
            imageElement3.FixedHeight = 85;
            imageElement3.FixedWidth = 85;
            imageElement3.ImageLayout = C1.Win.C1Tile.ForeImageLayout.ScaleInner;
            imageElement3.ImageSelector = C1.Win.C1Tile.ImageSelector.Image1;
            imageElement3.SymbolSize = C1.Win.C1Tile.SymbolSize.Image16x16;
            textElement4.Alignment = System.Drawing.ContentAlignment.BottomCenter;
            textElement4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            textElement4.Margin = new System.Windows.Forms.Padding(0, 0, 0, 6);
            this.template2.Elements.Add(imageElement2);
            this.template2.Elements.Add(imageElement3);
            this.template2.Elements.Add(textElement4);
            this.template2.Name = "template2";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))));
            this.pictureBox2.Location = new System.Drawing.Point(337, 110);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(41, 11, 0, 0);
            this.pictureBox2.Size = new System.Drawing.Size(958, 81);
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))));
            this.label1.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(375, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 41);
            this.label1.TabIndex = 13;
            this.label1.Text = "控件";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox3.Location = new System.Drawing.Point(0, 111);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(338, 81);
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // _backBtn
            // 
            this._backBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._backBtn.FlatAppearance.BorderSize = 0;
            this._backBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this._backBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._backBtn.Image = ((System.Drawing.Image)(resources.GetObject("_backBtn.Image")));
            this._backBtn.Location = new System.Drawing.Point(35, 112);
            this._backBtn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this._backBtn.Name = "_backBtn";
            this._backBtn.Size = new System.Drawing.Size(76, 79);
            this._backBtn.TabIndex = 16;
            this._backBtn.UseVisualStyleBackColor = false;
            this._backBtn.Visible = false;
            this._backBtn.Click += new System.EventHandler(this._backBtn_Click);
            // 
            // c1TileControl2
            // 
            this.c1TileControl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.c1TileControl2.CellHeight = 50;
            this.c1TileControl2.CellSpacing = 0;
            this.c1TileControl2.CellWidth = 280;
            // 
            // 
            // 
            textElement5.Alignment = System.Drawing.ContentAlignment.TopLeft;
            textElement5.Font = new System.Drawing.Font("微软雅黑", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            textElement5.ForeColor = System.Drawing.Color.Black;
            textElement5.Margin = new System.Windows.Forms.Padding(13, 4, 4, 1);
            textElement6.Alignment = System.Drawing.ContentAlignment.BottomLeft;
            textElement6.FixedHeight = 18;
            textElement6.FixedWidth = 243;
            textElement6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            textElement6.ForeColor = System.Drawing.Color.Black;
            textElement6.Margin = new System.Windows.Forms.Padding(13, 1, 4, 1);
            textElement6.TextSelector = C1.Win.C1Tile.TextSelector.Text1;
            this.c1TileControl2.DefaultTemplate.Elements.Add(textElement5);
            this.c1TileControl2.DefaultTemplate.Elements.Add(textElement6);
            this.c1TileControl2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1TileControl2.GroupPadding = new System.Windows.Forms.Padding(24, 0, 13, 6);
            this.c1TileControl2.Groups.Add(this.group2);
            this.c1TileControl2.GroupTextX = 24;
            this.c1TileControl2.GroupTextY = 6;
            this.c1TileControl2.HotBorderColor = System.Drawing.Color.Black;
            this.c1TileControl2.Location = new System.Drawing.Point(0, 192);
            this.c1TileControl2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.c1TileControl2.Name = "c1TileControl2";
            this.c1TileControl2.Orientation = C1.Win.C1Tile.LayoutOrientation.Vertical;
            this.c1TileControl2.Padding = new System.Windows.Forms.Padding(0, 37, 0, 0);
            this.c1TileControl2.Size = new System.Drawing.Size(338, 693);
            this.c1TileControl2.SurfacePadding = new System.Windows.Forms.Padding(18, 6, 18, 6);
            this.c1TileControl2.SwipeDistance = 31;
            this.c1TileControl2.SwipeRearrangeDistance = 151;
            this.c1TileControl2.TabIndex = 18;
            this.c1TileControl2.Templates.Add(this.template3);
            this.c1TileControl2.Templates.Add(this.template4);
            this.c1TileControl2.TextSize = 0F;
            this.c1TileControl2.TextX = 42;
            this.c1TileControl2.TextY = 18;
            this.c1TileControl2.TileBackColor = System.Drawing.Color.White;
            this.c1TileControl2.TileForeColor = System.Drawing.Color.Black;
            this.c1TileControl2.TileClicked += new System.EventHandler<C1.Win.C1Tile.TileEventArgs>(this.c1TileControl2_TileClicked);
            // 
            // group2
            // 
            this.group2.Name = "group2";
            this.group2.Tiles.Add(this.tile2);
            // 
            // tile2
            // 
            this.tile2.Name = "tile2";
            this.tile2.Template = this.template3;
            this.tile2.Text = "为界面的快速开发提供智能强有力的控件";
            this.tile2.VerticalSize = 12;
            // 
            // template3
            // 
            this.template3.Description = "MainPage";
            textElement7.Alignment = System.Drawing.ContentAlignment.TopLeft;
            textElement7.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            textElement7.Margin = new System.Windows.Forms.Padding(10, 18, 0, 0);
            textElement8.Alignment = System.Drawing.ContentAlignment.TopLeft;
            textElement8.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            textElement8.Margin = new System.Windows.Forms.Padding(12, 195, 12, 0);
            textElement8.TextSelector = C1.Win.C1Tile.TextSelector.Text1;
            this.template3.Elements.Add(textElement7);
            this.template3.Elements.Add(textElement8);
            this.template3.Name = "template3";
            // 
            // template4
            // 
            this.template4.Description = "NewSample";
            imageElement4.Alignment = System.Drawing.ContentAlignment.TopLeft;
            imageElement4.FixedHeight = 13;
            imageElement4.FixedWidth = 13;
            imageElement4.SymbolSize = C1.Win.C1Tile.SymbolSize.Image16x16;
            textElement9.Alignment = System.Drawing.ContentAlignment.TopLeft;
            textElement9.Font = new System.Drawing.Font("微软雅黑", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            textElement9.Margin = new System.Windows.Forms.Padding(13, 4, 4, 1);
            textElement10.Alignment = System.Drawing.ContentAlignment.BottomLeft;
            textElement10.FixedHeight = 18;
            textElement10.FixedWidth = 243;
            textElement10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            textElement10.Margin = new System.Windows.Forms.Padding(13, 1, 4, 1);
            textElement10.TextSelector = C1.Win.C1Tile.TextSelector.Text1;
            this.template4.Elements.Add(imageElement4);
            this.template4.Elements.Add(textElement9);
            this.template4.Elements.Add(textElement10);
            this.template4.Name = "template4";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(372, 192);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1072, 42);
            this.panel2.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "label2";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(35, 18, 0, 0);
            this.pictureBox1.Size = new System.Drawing.Size(1283, 111);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))));
            this.label3.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(455, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 41);
            this.label3.TabIndex = 20;
            this.label3.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1283, 926);
            this.Controls.Add(this.c1StatusBar1);
            this.Controls.Add(this.c1TileControl1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c1TileControl2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this._backBtn);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.c1InputPanel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "MainForm";
            this.Text = "Studio for WinForms Control Explorer";
            this.VisualStyleHolder = C1.Win.C1Ribbon.VisualStyle.Office2010Silver;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c1StatusBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1InputPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList controlIcons;
        private C1.Win.C1Win7Pack.C1TaskbarButton c1TaskbarButton1;
        private System.Windows.Forms.Panel panel1;
        private C1.Win.C1InputPanel.C1InputPanel c1InputPanel1;
        private C1.Win.C1InputPanel.InputLabel vsLabel;
        private C1.Win.C1InputPanel.InputComboBox comboVisualStyles;
        private C1.Win.C1InputPanel.InputOption office2007BlueOption;
        private C1.Win.C1InputPanel.InputOption office2007SilverOption;
        private C1.Win.C1InputPanel.InputOption office2007BlackOption;
        private C1.Win.C1InputPanel.InputOption office2010BlueOption;
        private C1.Win.C1InputPanel.InputOption office2010SilverOption;
        private C1.Win.C1InputPanel.InputOption office2010BlackOption;
        private C1.Win.C1Ribbon.C1StatusBar c1StatusBar1;
        private System.Windows.Forms.ImageList sampleIcons;
        private C1.Win.C1InputPanel.InputOption windowsOption;
        private System.Windows.Forms.ImageList stateImage;
        private System.Windows.Forms.PictureBox pictureBox2;
        private C1.Win.C1Tile.C1TileControl c1TileControl1;
        private C1.Win.C1Tile.Template template1;
        private C1.Win.C1Tile.Group controls;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _backBtn;
        private System.Windows.Forms.PictureBox pictureBox3;
        private C1.Win.C1Tile.Template template2;
        private C1.Win.C1Tile.C1TileControl c1TileControl2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private C1.Win.C1Tile.Group group2;
        private C1.Win.C1Tile.Tile tile2;
        private C1.Win.C1Tile.Template template3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private C1.Win.C1Tile.Template template4;

    }
}

