﻿namespace ControlExplorer.Input
{
    partial class FormatTypes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.c1Magnify1 = new C1.Win.TouchToolKit.C1Magnify();
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.c1de_Time = new C1.Win.C1Input.C1DateEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.c1de_General = new C1.Win.C1Input.C1DateEdit();
            this.c1de_GMT = new C1.Win.C1Input.C1DateEdit();
            this.c1de_LongDate = new C1.Win.C1Input.C1DateEdit();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.c1tb_Time = new C1.Win.C1Input.C1TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.c1tb_OnOff = new C1.Win.C1Input.C1TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.c1tb_YesNo = new C1.Win.C1Input.C1TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.c1tb_MonthDay = new C1.Win.C1Input.C1TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.c1tb_Currency = new C1.Win.C1Input.C1TextBox();
            this.c1tb_StandardNumber = new C1.Win.C1Input.C1TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.c1tb_EditMask = new C1.Win.C1Input.C1TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.c1tb_Hexadecimal = new C1.Win.C1Input.C1TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c1tb_RegularText = new C1.Win.C1Input.C1TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.c1ne_Integer = new C1.Win.C1Input.C1NumericEdit();
            this.c1ne_Currency = new C1.Win.C1Input.C1NumericEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.c1ne_Scientific = new C1.Win.C1Input.C1NumericEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.c1ne_Percent = new C1.Win.C1Input.C1NumericEdit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1de_Time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1de_General)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1de_GMT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1de_LongDate)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_Time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_OnOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_YesNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_MonthDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_Currency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_StandardNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_EditMask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_Hexadecimal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_RegularText)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1ne_Integer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ne_Currency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ne_Scientific)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ne_Percent)).BeginInit();
            this.SuspendLayout();
            // 
            // c1Magnify1
            // 
            this.c1Magnify1.PopupDelay = 200;
            this.c1Magnify1.Shape = C1.Win.TouchToolKit.MagnifierShape.Rectangle;
            this.c1Magnify1.Size = new System.Drawing.Size(250, 250);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.c1de_Time);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.c1de_General);
            this.groupBox2.Controls.Add(this.c1de_GMT);
            this.groupBox2.Controls.Add(this.c1de_LongDate);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.groupBox2.Location = new System.Drawing.Point(266, 197);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(263, 209);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "C1日期编辑";
            this.c1ThemeController1.SetTheme(this.groupBox2, "(default)");
            // 
            // c1de_Time
            // 
            this.c1de_Time.AutoOpen = false;
            this.c1de_Time.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1de_Time.Calendar.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_Time.Calendar.BackColor = System.Drawing.Color.White;
            this.c1de_Time.Calendar.DayNamesColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_Time.Calendar.DayNamesFont = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_Time.Calendar.Font = new System.Drawing.Font("Tahoma", 8F);
            this.c1de_Time.Calendar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_Time.Calendar.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(179)))), ((int)(((byte)(245)))));
            this.c1de_Time.Calendar.SelectionForeColor = System.Drawing.Color.White;
            this.c1de_Time.Calendar.TitleBackColor = System.Drawing.Color.White;
            this.c1de_Time.Calendar.TitleFont = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_Time.Calendar.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_Time.Calendar.TodayBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(179)))), ((int)(((byte)(245)))));
            this.c1de_Time.Calendar.TrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(152)))), ((int)(((byte)(152)))));
            this.c1de_Time.Calendar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            this.c1de_Time.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_Time.FormatType = C1.Win.C1Input.FormatTypeEnum.MediumTime;
            this.c1de_Time.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1de_Time.Location = new System.Drawing.Point(94, 171);
            this.c1de_Time.Margin = new System.Windows.Forms.Padding(4);
            this.c1de_Time.Name = "c1de_Time";
            this.c1de_Time.Size = new System.Drawing.Size(157, 21);
            this.c1de_Time.TabIndex = 7;
            this.c1de_Time.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1de_Time, "(default)");
            this.c1de_Time.Value = new System.DateTime(2011, 2, 8, 10, 15, 30, 0);
            this.c1de_Time.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.UpDown;
            this.c1de_Time.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.White;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label18.Location = new System.Drawing.Point(17, 175);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 17);
            this.label18.TabIndex = 6;
            this.label18.Text = "时间:";
            this.c1ThemeController1.SetTheme(this.label18, "(default)");
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.White;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label17.Location = new System.Drawing.Point(15, 132);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 17);
            this.label17.TabIndex = 5;
            this.label17.Text = "一般日期:";
            this.c1ThemeController1.SetTheme(this.label17, "(default)");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.White;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label16.Location = new System.Drawing.Point(15, 86);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 17);
            this.label16.TabIndex = 4;
            this.label16.Text = "时间(GMT):";
            this.c1ThemeController1.SetTheme(this.label16, "(default)");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.White;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label15.Location = new System.Drawing.Point(17, 40);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 17);
            this.label15.TabIndex = 3;
            this.label15.Text = "长日期:";
            this.c1ThemeController1.SetTheme(this.label15, "(default)");
            // 
            // c1de_General
            // 
            this.c1de_General.AutoOpen = false;
            this.c1de_General.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1de_General.Calendar.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_General.Calendar.BackColor = System.Drawing.Color.White;
            this.c1de_General.Calendar.ClearText = "清除（&C）";
            this.c1de_General.Calendar.DayNamesColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_General.Calendar.DayNamesFont = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_General.Calendar.Font = new System.Drawing.Font("Tahoma", 8F);
            this.c1de_General.Calendar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_General.Calendar.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(179)))), ((int)(((byte)(245)))));
            this.c1de_General.Calendar.SelectionForeColor = System.Drawing.Color.White;
            this.c1de_General.Calendar.TitleBackColor = System.Drawing.Color.White;
            this.c1de_General.Calendar.TitleFont = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_General.Calendar.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_General.Calendar.TodayBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(179)))), ((int)(((byte)(245)))));
            this.c1de_General.Calendar.TodayText = "今天（&T）";
            this.c1de_General.Calendar.TrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(152)))), ((int)(((byte)(152)))));
            this.c1de_General.Calendar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            this.c1de_General.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_General.FormatType = C1.Win.C1Input.FormatTypeEnum.GeneralDate;
            this.c1de_General.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1de_General.Location = new System.Drawing.Point(94, 127);
            this.c1de_General.Margin = new System.Windows.Forms.Padding(4);
            this.c1de_General.Name = "c1de_General";
            this.c1de_General.Size = new System.Drawing.Size(159, 21);
            this.c1de_General.TabIndex = 2;
            this.c1de_General.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1de_General, "(default)");
            this.c1de_General.Value = new System.DateTime(2003, 6, 10, 0, 0, 0, 0);
            this.c1de_General.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // c1de_GMT
            // 
            this.c1de_GMT.AutoOpen = false;
            this.c1de_GMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1de_GMT.Calendar.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_GMT.Calendar.BackColor = System.Drawing.Color.White;
            this.c1de_GMT.Calendar.ClearText = "清除（&C）";
            this.c1de_GMT.Calendar.DayNamesColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_GMT.Calendar.DayNamesFont = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_GMT.Calendar.Font = new System.Drawing.Font("Tahoma", 8F);
            this.c1de_GMT.Calendar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_GMT.Calendar.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(179)))), ((int)(((byte)(245)))));
            this.c1de_GMT.Calendar.SelectionForeColor = System.Drawing.Color.White;
            this.c1de_GMT.Calendar.TitleBackColor = System.Drawing.Color.White;
            this.c1de_GMT.Calendar.TitleFont = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_GMT.Calendar.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_GMT.Calendar.TodayBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(179)))), ((int)(((byte)(245)))));
            this.c1de_GMT.Calendar.TodayText = "今天（&T）";
            this.c1de_GMT.Calendar.TrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(152)))), ((int)(((byte)(152)))));
            this.c1de_GMT.Calendar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            this.c1de_GMT.Culture = 2052;
            this.c1de_GMT.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_GMT.FormatType = C1.Win.C1Input.FormatTypeEnum.GMTSortable;
            this.c1de_GMT.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1de_GMT.Location = new System.Drawing.Point(94, 83);
            this.c1de_GMT.Margin = new System.Windows.Forms.Padding(4);
            this.c1de_GMT.Name = "c1de_GMT";
            this.c1de_GMT.Size = new System.Drawing.Size(159, 21);
            this.c1de_GMT.TabIndex = 1;
            this.c1de_GMT.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1de_GMT, "(default)");
            this.c1de_GMT.Value = new System.DateTime(2010, 11, 15, 23, 45, 0, 0);
            this.c1de_GMT.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // c1de_LongDate
            // 
            this.c1de_LongDate.AutoOpen = false;
            this.c1de_LongDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1de_LongDate.Calendar.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_LongDate.Calendar.BackColor = System.Drawing.Color.White;
            this.c1de_LongDate.Calendar.ClearText = "清除（&C）";
            this.c1de_LongDate.Calendar.DayNamesColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_LongDate.Calendar.DayNamesFont = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_LongDate.Calendar.Font = new System.Drawing.Font("Tahoma", 8F);
            this.c1de_LongDate.Calendar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_LongDate.Calendar.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(179)))), ((int)(((byte)(245)))));
            this.c1de_LongDate.Calendar.SelectionForeColor = System.Drawing.Color.White;
            this.c1de_LongDate.Calendar.TitleBackColor = System.Drawing.Color.White;
            this.c1de_LongDate.Calendar.TitleFont = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_LongDate.Calendar.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1de_LongDate.Calendar.TodayBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(179)))), ((int)(((byte)(245)))));
            this.c1de_LongDate.Calendar.TodayText = "今天（&T）";
            this.c1de_LongDate.Calendar.TrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(152)))), ((int)(((byte)(152)))));
            this.c1de_LongDate.Calendar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            this.c1de_LongDate.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1de_LongDate.FormatType = C1.Win.C1Input.FormatTypeEnum.LongDate;
            this.c1de_LongDate.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1de_LongDate.Location = new System.Drawing.Point(94, 36);
            this.c1de_LongDate.Margin = new System.Windows.Forms.Padding(4);
            this.c1de_LongDate.Name = "c1de_LongDate";
            this.c1de_LongDate.Size = new System.Drawing.Size(159, 21);
            this.c1de_LongDate.TabIndex = 0;
            this.c1de_LongDate.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1de_LongDate, "(default)");
            this.c1de_LongDate.Value = new System.DateTime(2011, 2, 3, 11, 29, 27, 0);
            this.c1de_LongDate.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.c1tb_Time);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.c1tb_OnOff);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.c1tb_YesNo);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.c1tb_MonthDay);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.c1tb_Currency);
            this.groupBox1.Controls.Add(this.c1tb_StandardNumber);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.c1tb_EditMask);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.c1tb_Hexadecimal);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.c1tb_RegularText);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.groupBox1.Location = new System.Drawing.Point(14, 16);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(244, 390);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "C1文本框";
            this.c1ThemeController1.SetTheme(this.groupBox1, "(default)");
            // 
            // c1tb_Time
            // 
            this.c1tb_Time.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1tb_Time.DataType = typeof(System.DateTime);
            this.c1tb_Time.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1tb_Time.FormatType = C1.Win.C1Input.FormatTypeEnum.LongTime;
            this.c1tb_Time.Location = new System.Drawing.Point(85, 353);
            this.c1tb_Time.Margin = new System.Windows.Forms.Padding(0);
            this.c1tb_Time.Name = "c1tb_Time";
            this.c1tb_Time.Size = new System.Drawing.Size(144, 21);
            this.c1tb_Time.TabIndex = 22;
            this.c1tb_Time.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1tb_Time, "(default)");
            this.c1tb_Time.Value = new System.DateTime(2011, 2, 3, 12, 51, 10, 0);
            this.c1tb_Time.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.White;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label14.Location = new System.Drawing.Point(18, 357);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 17);
            this.label14.TabIndex = 21;
            this.label14.Text = "长时间:";
            this.c1ThemeController1.SetTheme(this.label14, "(default)");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.White;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label13.Location = new System.Drawing.Point(18, 311);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 17);
            this.label13.TabIndex = 20;
            this.label13.Text = "开/关:";
            this.c1ThemeController1.SetTheme(this.label13, "(default)");
            // 
            // c1tb_OnOff
            // 
            this.c1tb_OnOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1tb_OnOff.DataType = typeof(bool);
            this.c1tb_OnOff.ErrorInfo.ErrorMessage = "User may only enter \'On\' (true) or \'Off\' (false).";
            this.c1tb_OnOff.ErrorInfo.ErrorProvider = this.errorProvider1;
            this.c1tb_OnOff.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1tb_OnOff.FormatType = C1.Win.C1Input.FormatTypeEnum.OnOff;
            this.c1tb_OnOff.Location = new System.Drawing.Point(85, 308);
            this.c1tb_OnOff.Margin = new System.Windows.Forms.Padding(0);
            this.c1tb_OnOff.Name = "c1tb_OnOff";
            this.c1tb_OnOff.Size = new System.Drawing.Size(144, 21);
            this.c1tb_OnOff.TabIndex = 19;
            this.c1tb_OnOff.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1tb_OnOff, "(default)");
            this.c1tb_OnOff.Value = false;
            this.c1tb_OnOff.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label2.Location = new System.Drawing.Point(18, 107);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "是/否:";
            this.c1ThemeController1.SetTheme(this.label2, "(default)");
            // 
            // c1tb_YesNo
            // 
            this.c1tb_YesNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1tb_YesNo.DataType = typeof(bool);
            this.c1tb_YesNo.ErrorInfo.ErrorMessage = "Value should be \'Yes\' or \'No\'";
            this.c1tb_YesNo.ErrorInfo.ErrorProvider = this.errorProvider1;
            this.c1tb_YesNo.ErrorInfo.ValueOnError = true;
            this.c1tb_YesNo.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1tb_YesNo.FormatType = C1.Win.C1Input.FormatTypeEnum.YesNo;
            this.c1tb_YesNo.Location = new System.Drawing.Point(85, 103);
            this.c1tb_YesNo.Margin = new System.Windows.Forms.Padding(0);
            this.c1tb_YesNo.MaskInfo.ErrorMessage = "值必须是 “是” 或者 “否”";
            this.c1tb_YesNo.MaskInfo.Inherit = ((C1.Win.C1Input.MaskInfoInheritFlags)((C1.Win.C1Input.MaskInfoInheritFlags.CaseSensitive | C1.Win.C1Input.MaskInfoInheritFlags.EmptyAsNull)));
            this.c1tb_YesNo.Name = "c1tb_YesNo";
            this.c1tb_YesNo.NumericInput = false;
            this.c1tb_YesNo.Size = new System.Drawing.Size(144, 21);
            this.c1tb_YesNo.TabIndex = 3;
            this.c1tb_YesNo.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1tb_YesNo, "(default)");
            this.c1tb_YesNo.Value = true;
            this.c1tb_YesNo.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.White;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label12.Location = new System.Drawing.Point(18, 220);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "月/日:";
            this.c1ThemeController1.SetTheme(this.label12, "(default)");
            // 
            // c1tb_MonthDay
            // 
            this.c1tb_MonthDay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1tb_MonthDay.DataType = typeof(System.DateTime);
            this.c1tb_MonthDay.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1tb_MonthDay.FormatType = C1.Win.C1Input.FormatTypeEnum.MonthAndDay;
            this.c1tb_MonthDay.Location = new System.Drawing.Point(85, 216);
            this.c1tb_MonthDay.Margin = new System.Windows.Forms.Padding(0);
            this.c1tb_MonthDay.Name = "c1tb_MonthDay";
            this.c1tb_MonthDay.Size = new System.Drawing.Size(144, 21);
            this.c1tb_MonthDay.TabIndex = 17;
            this.c1tb_MonthDay.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1tb_MonthDay, "(default)");
            this.c1tb_MonthDay.Value = new System.DateTime(2011, 2, 3, 12, 43, 39, 0);
            this.c1tb_MonthDay.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label9.Location = new System.Drawing.Point(18, 265);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 17);
            this.label9.TabIndex = 14;
            this.label9.Text = "货币:";
            this.c1ThemeController1.SetTheme(this.label9, "(default)");
            // 
            // c1tb_Currency
            // 
            this.c1tb_Currency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1tb_Currency.DataType = typeof(double);
            this.c1tb_Currency.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1tb_Currency.FormatType = C1.Win.C1Input.FormatTypeEnum.Currency;
            this.c1tb_Currency.Location = new System.Drawing.Point(85, 262);
            this.c1tb_Currency.Margin = new System.Windows.Forms.Padding(0);
            this.c1tb_Currency.Name = "c1tb_Currency";
            this.c1tb_Currency.Size = new System.Drawing.Size(144, 21);
            this.c1tb_Currency.TabIndex = 13;
            this.c1tb_Currency.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1tb_Currency, "(default)");
            this.c1tb_Currency.Value = 785.95D;
            this.c1tb_Currency.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // c1tb_StandardNumber
            // 
            this.c1tb_StandardNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1tb_StandardNumber.DataType = typeof(int);
            this.c1tb_StandardNumber.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1tb_StandardNumber.FormatType = C1.Win.C1Input.FormatTypeEnum.StandardNumber;
            this.c1tb_StandardNumber.Location = new System.Drawing.Point(85, 177);
            this.c1tb_StandardNumber.Margin = new System.Windows.Forms.Padding(0);
            this.c1tb_StandardNumber.Name = "c1tb_StandardNumber";
            this.c1tb_StandardNumber.Size = new System.Drawing.Size(144, 21);
            this.c1tb_StandardNumber.TabIndex = 10;
            this.c1tb_StandardNumber.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1tb_StandardNumber, "(default)");
            this.c1tb_StandardNumber.Value = 35000;
            this.c1tb_StandardNumber.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label7.Location = new System.Drawing.Point(18, 180);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 17);
            this.label7.TabIndex = 9;
            this.label7.Text = "标准编号:";
            this.c1ThemeController1.SetTheme(this.label7, "(default)");
            // 
            // c1tb_EditMask
            // 
            this.c1tb_EditMask.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1tb_EditMask.EditMask = "(999) 0099-0099";
            this.c1tb_EditMask.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1tb_EditMask.Location = new System.Drawing.Point(85, 69);
            this.c1tb_EditMask.Margin = new System.Windows.Forms.Padding(0);
            this.c1tb_EditMask.Name = "c1tb_EditMask";
            this.c1tb_EditMask.Size = new System.Drawing.Size(144, 21);
            this.c1tb_EditMask.TabIndex = 8;
            this.c1tb_EditMask.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1tb_EditMask, "(default)");
            this.c1tb_EditMask.Value = "(029)8833-1988";
            this.c1tb_EditMask.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label3.Location = new System.Drawing.Point(18, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "输入掩码:";
            this.c1ThemeController1.SetTheme(this.label3, "(default)");
            // 
            // c1tb_Hexadecimal
            // 
            this.c1tb_Hexadecimal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1tb_Hexadecimal.DataType = typeof(int);
            this.c1tb_Hexadecimal.ErrorInfo.ErrorMessage = "User may only type digits 0-9, letters A-F, and the minus sign (or change the sig" +
    "n of the number by pressing F9).";
            this.c1tb_Hexadecimal.ErrorInfo.ErrorProvider = this.errorProvider1;
            this.c1tb_Hexadecimal.ErrorInfo.ValueOnError = 0;
            this.c1tb_Hexadecimal.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1tb_Hexadecimal.FormatType = C1.Win.C1Input.FormatTypeEnum.Hexadecimal;
            this.c1tb_Hexadecimal.Location = new System.Drawing.Point(85, 137);
            this.c1tb_Hexadecimal.Margin = new System.Windows.Forms.Padding(0);
            this.c1tb_Hexadecimal.Name = "c1tb_Hexadecimal";
            this.c1tb_Hexadecimal.Size = new System.Drawing.Size(144, 21);
            this.c1tb_Hexadecimal.TabIndex = 6;
            this.c1tb_Hexadecimal.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1tb_Hexadecimal, "(default)");
            this.c1tb_Hexadecimal.Value = 16776960;
            this.c1tb_Hexadecimal.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label4.Location = new System.Drawing.Point(18, 140);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "十六进制:";
            this.c1ThemeController1.SetTheme(this.label4, "(default)");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label1.Location = new System.Drawing.Point(18, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "默认格式:";
            this.c1ThemeController1.SetTheme(this.label1, "(default)");
            // 
            // c1tb_RegularText
            // 
            this.c1tb_RegularText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1tb_RegularText.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1tb_RegularText.Location = new System.Drawing.Point(85, 34);
            this.c1tb_RegularText.Margin = new System.Windows.Forms.Padding(4);
            this.c1tb_RegularText.Name = "c1tb_RegularText";
            this.c1tb_RegularText.Size = new System.Drawing.Size(144, 21);
            this.c1tb_RegularText.TabIndex = 0;
            this.c1tb_RegularText.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1tb_RegularText, "(default)");
            this.c1tb_RegularText.Value = "c1TextBox 规则!";
            this.c1tb_RegularText.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.c1ne_Integer);
            this.groupBox3.Controls.Add(this.c1ne_Currency);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.c1ne_Scientific);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.c1ne_Percent);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.groupBox3.Location = new System.Drawing.Point(266, 17);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(263, 172);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "C1数字编辑";
            this.c1ThemeController1.SetTheme(this.groupBox3, "(default)");
            // 
            // c1ne_Integer
            // 
            this.c1ne_Integer.AutoOpen = false;
            this.c1ne_Integer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1ne_Integer.Calculator.BackColor = System.Drawing.Color.White;
            this.c1ne_Integer.Calculator.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            this.c1ne_Integer.DataType = typeof(int);
            this.c1ne_Integer.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1ne_Integer.FormatType = C1.Win.C1Input.FormatTypeEnum.Integer;
            this.c1ne_Integer.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1ne_Integer.Location = new System.Drawing.Point(82, 136);
            this.c1ne_Integer.Margin = new System.Windows.Forms.Padding(4);
            this.c1ne_Integer.Name = "c1ne_Integer";
            this.c1ne_Integer.Size = new System.Drawing.Size(169, 21);
            this.c1ne_Integer.TabIndex = 12;
            this.c1ne_Integer.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1ne_Integer, "(default)");
            this.c1ne_Integer.Value = -5;
            this.c1ne_Integer.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.UpDown;
            this.c1ne_Integer.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // c1ne_Currency
            // 
            this.c1ne_Currency.AutoOpen = false;
            this.c1ne_Currency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1ne_Currency.Calculator.BackColor = System.Drawing.Color.White;
            this.c1ne_Currency.Calculator.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            this.c1ne_Currency.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1ne_Currency.FormatType = C1.Win.C1Input.FormatTypeEnum.Currency;
            this.c1ne_Currency.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1ne_Currency.Location = new System.Drawing.Point(82, 103);
            this.c1ne_Currency.Margin = new System.Windows.Forms.Padding(4);
            this.c1ne_Currency.Name = "c1ne_Currency";
            this.c1ne_Currency.Size = new System.Drawing.Size(169, 21);
            this.c1ne_Currency.TabIndex = 5;
            this.c1ne_Currency.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1ne_Currency, "(default)");
            this.c1ne_Currency.Value = new decimal(new int[] {
            15838,
            0,
            0,
            131072});
            this.c1ne_Currency.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.UpDown;
            this.c1ne_Currency.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label10.Location = new System.Drawing.Point(17, 106);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "货币:";
            this.c1ThemeController1.SetTheme(this.label10, "(default)");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label6.Location = new System.Drawing.Point(15, 73);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "科学计数:";
            this.c1ThemeController1.SetTheme(this.label6, "(default)");
            // 
            // c1ne_Scientific
            // 
            this.c1ne_Scientific.AutoOpen = false;
            this.c1ne_Scientific.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1ne_Scientific.Calculator.BackColor = System.Drawing.Color.White;
            this.c1ne_Scientific.Calculator.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            this.c1ne_Scientific.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1ne_Scientific.FormatType = C1.Win.C1Input.FormatTypeEnum.Scientific;
            this.c1ne_Scientific.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1ne_Scientific.Location = new System.Drawing.Point(82, 69);
            this.c1ne_Scientific.Margin = new System.Windows.Forms.Padding(4);
            this.c1ne_Scientific.Name = "c1ne_Scientific";
            this.c1ne_Scientific.Size = new System.Drawing.Size(169, 21);
            this.c1ne_Scientific.TabIndex = 2;
            this.c1ne_Scientific.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1ne_Scientific, "(default)");
            this.c1ne_Scientific.Value = new decimal(new int[] {
            36870000,
            0,
            0,
            0});
            this.c1ne_Scientific.VisibleButtons = ((C1.Win.C1Input.DropDownControlButtonFlags)(((C1.Win.C1Input.DropDownControlButtonFlags.UpDown | C1.Win.C1Input.DropDownControlButtonFlags.DropDown) 
            | C1.Win.C1Input.DropDownControlButtonFlags.Modal)));
            this.c1ne_Scientific.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label8.Location = new System.Drawing.Point(17, 138);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 17);
            this.label8.TabIndex = 11;
            this.label8.Text = "整数:";
            this.c1ThemeController1.SetTheme(this.label8, "(default)");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label5.Location = new System.Drawing.Point(17, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "百分数:";
            this.c1ThemeController1.SetTheme(this.label5, "(default)");
            // 
            // c1ne_Percent
            // 
            this.c1ne_Percent.AutoOpen = false;
            this.c1ne_Percent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1ne_Percent.Calculator.BackColor = System.Drawing.Color.White;
            this.c1ne_Percent.Calculator.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            this.c1ne_Percent.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.c1ne_Percent.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.c1ne_Percent.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1ne_Percent.Increment = new decimal(new int[] {
            5,
            0,
            0,
            262144});
            this.c1ne_Percent.Location = new System.Drawing.Point(82, 31);
            this.c1ne_Percent.Margin = new System.Windows.Forms.Padding(4);
            this.c1ne_Percent.Name = "c1ne_Percent";
            this.c1ne_Percent.Size = new System.Drawing.Size(169, 21);
            this.c1ne_Percent.TabIndex = 0;
            this.c1ne_Percent.Tag = null;
            this.c1ThemeController1.SetTheme(this.c1ne_Percent, "(default)");
            this.c1ne_Percent.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.c1ne_Percent.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2010Blue;
            // 
            // FormatTypes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(543, 421);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.c1Magnify1.SetEnableMagnifier(this, true);
            this.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormatTypes";
            this.Text = "新概述";
            this.c1ThemeController1.SetTheme(this, "(default)");
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1de_Time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1de_General)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1de_GMT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1de_LongDate)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_Time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_OnOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_YesNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_MonthDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_Currency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_StandardNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_EditMask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_Hexadecimal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1tb_RegularText)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1ne_Integer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ne_Currency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ne_Scientific)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ne_Percent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.BindingSource bindingSource1;
        private C1.Win.C1Input.C1TextBox c1tb_RegularText;
        private System.Windows.Forms.Label label1;
        private C1.Win.C1Input.C1TextBox c1tb_YesNo;
        private System.Windows.Forms.Label label2;
        private C1.Win.C1Input.C1TextBox c1tb_Hexadecimal;
        private System.Windows.Forms.Label label4;
        private C1.Win.C1Input.C1TextBox c1tb_EditMask;
        private System.Windows.Forms.Label label3;
        private C1.Win.C1Input.C1NumericEdit c1ne_Percent;
        private System.Windows.Forms.Label label5;
        private C1.Win.C1Input.C1TextBox c1tb_StandardNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private C1.Win.C1Input.C1NumericEdit c1ne_Scientific;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private C1.Win.C1Input.C1TextBox c1tb_Currency;
        private C1.Win.C1Input.C1NumericEdit c1ne_Currency;
        private System.Windows.Forms.Label label10;
        private C1.Win.C1Input.C1NumericEdit c1ne_Integer;
        private System.Windows.Forms.Label label12;
        private C1.Win.C1Input.C1TextBox c1tb_MonthDay;
        private C1.Win.C1Input.C1TextBox c1tb_OnOff;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private C1.Win.C1Input.C1TextBox c1tb_Time;
        private C1.Win.C1Input.C1DateEdit c1de_General;
        private C1.Win.C1Input.C1DateEdit c1de_GMT;
        private C1.Win.C1Input.C1DateEdit c1de_LongDate;
        private C1.Win.C1Input.C1DateEdit c1de_Time;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private C1.Win.TouchToolKit.C1Magnify c1Magnify1;
        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;
    }
}