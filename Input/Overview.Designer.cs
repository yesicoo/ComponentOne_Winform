﻿namespace ControlExplorer.Input
{
    partial class Overview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.c1TextBox1 = new C1.Win.C1Input.C1TextBox();
            this.employeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.c1DemoDataSet = new ControlExplorer.C1DemoDataSet();
            this.c1DateEdit1 = new C1.Win.C1Input.C1DateEdit();
            this.c1Label1 = new C1.Win.C1Input.C1Label();
            this.c1DbNavigator1 = new C1.Win.C1Input.C1DbNavigator();
            this.c1NumericEdit1 = new C1.Win.C1Input.C1NumericEdit();
            this.c1TextBox2 = new C1.Win.C1Input.C1TextBox();
            this.c1TextBox3 = new C1.Win.C1Input.C1TextBox();
            this.c1DropDownControl1 = new C1.Win.C1Input.C1DropDownControl();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.c1Button1 = new C1.Win.C1Input.C1Button();
            this.employeesTableAdapter = new ControlExplorer.C1DemoDataSetTableAdapters.EmployeesTableAdapter();
            this.c1PictureBox1 = new C1.Win.C1Input.C1PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.c1CheckBox1 = new C1.Win.C1Input.C1CheckBox();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.c1TextBox5 = new C1.Win.C1Input.C1TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.c1NumericEdit4 = new C1.Win.C1Input.C1NumericEdit();
            this.c1NumericEdit3 = new C1.Win.C1Input.C1NumericEdit();
            this.c1NumericEdit2 = new C1.Win.C1Input.C1NumericEdit();
            this.c1Label2 = new C1.Win.C1Input.C1Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.c1TextBox4 = new C1.Win.C1Input.C1TextBox();
            this.c1DbNavigator2 = new C1.Win.C1Input.C1DbNavigator();
            this.productsTableAdapter = new ControlExplorer.C1DemoDataSetTableAdapters.ProductsTableAdapter();
            this.c1Magnify1 = new C1.Win.TouchToolKit.C1Magnify();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DemoDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DbNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DropDownControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1PictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DbNavigator2)).BeginInit();
            this.SuspendLayout();
            // 
            // c1TextBox1
            // 
            this.c1TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1TextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.employeesBindingSource, "FirstName", true));
            this.c1TextBox1.DateTimeInput = false;
            this.c1TextBox1.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.FormatType | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.c1TextBox1.Location = new System.Drawing.Point(288, 39);
            this.c1TextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.c1TextBox1.Name = "c1TextBox1";
            this.c1TextBox1.NumericInput = false;
            this.c1TextBox1.Size = new System.Drawing.Size(196, 21);
            this.c1TextBox1.TabIndex = 0;
            this.c1TextBox1.Tag = null;
            this.c1TextBox1.VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1TextBox1.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            // 
            // employeesBindingSource
            // 
            this.employeesBindingSource.DataMember = "Employees";
            this.employeesBindingSource.DataSource = this.c1DemoDataSet;
            // 
            // c1DemoDataSet
            // 
            this.c1DemoDataSet.DataSetName = "C1DemoDataSet";
            this.c1DemoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // c1DateEdit1
            // 
            this.c1DateEdit1.AutoOpen = false;
            this.c1DateEdit1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1DateEdit1.Calendar.Font = new System.Drawing.Font("Tahoma", 8F);
            this.c1DateEdit1.Calendar.VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1DateEdit1.Calendar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1DateEdit1.CustomFormat = "d";
            this.c1DateEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.employeesBindingSource, "BirthDate", true));
            this.c1DateEdit1.DisplayFormat.CustomFormat = "d";
            this.c1DateEdit1.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.c1DateEdit1.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)((((C1.Win.C1Input.FormatInfoInheritFlags.NullText | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.c1DateEdit1.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1DateEdit1.Location = new System.Drawing.Point(288, 141);
            this.c1DateEdit1.Margin = new System.Windows.Forms.Padding(4);
            this.c1DateEdit1.Name = "c1DateEdit1";
            this.c1DateEdit1.Size = new System.Drawing.Size(196, 21);
            this.c1DateEdit1.TabIndex = 1;
            this.c1DateEdit1.Tag = null;
            this.c1DateEdit1.VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1DateEdit1.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            // 
            // c1Label1
            // 
            this.c1Label1.AutoSize = true;
            this.c1Label1.BackColor = System.Drawing.Color.Transparent;
            this.c1Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c1Label1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.employeesBindingSource, "EmployeeID", true));
            this.c1Label1.DataType = typeof(int);
            this.c1Label1.ForeColor = System.Drawing.Color.Black;
            this.c1Label1.FormatInfo.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.FormatType | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.c1Label1.Location = new System.Drawing.Point(288, 6);
            this.c1Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.c1Label1.Name = "c1Label1";
            this.c1Label1.Size = new System.Drawing.Size(59, 17);
            this.c1Label1.TabIndex = 2;
            this.c1Label1.Tag = null;
            this.c1Label1.VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1Label1.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            // 
            // c1DbNavigator1
            // 
            this.c1DbNavigator1.AutoSize = false;
            this.c1DbNavigator1.ColorWhenHover = false;
            this.c1DbNavigator1.DataSource = this.employeesBindingSource;
            this.c1DbNavigator1.Location = new System.Drawing.Point(8, 293);
            this.c1DbNavigator1.Margin = new System.Windows.Forms.Padding(4);
            this.c1DbNavigator1.Name = "c1DbNavigator1";
            this.c1DbNavigator1.Size = new System.Drawing.Size(277, 33);
            this.c1DbNavigator1.TabIndex = 5;
            this.c1DbNavigator1.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1DbNavigator1.Error += new C1.Win.C1Input.NavigatorErrorEventHandler(this.c1DbNavigator1_Error);
            // 
            // c1NumericEdit1
            // 
            this.c1NumericEdit1.AutoOpen = false;
            this.c1NumericEdit1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1NumericEdit1.Calculator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.c1NumericEdit1.Calculator.VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1NumericEdit1.Calculator.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1NumericEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.employeesBindingSource, "Extension", true));
            this.c1NumericEdit1.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.FormatType | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.c1NumericEdit1.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1NumericEdit1.Location = new System.Drawing.Point(288, 175);
            this.c1NumericEdit1.Margin = new System.Windows.Forms.Padding(4);
            this.c1NumericEdit1.Name = "c1NumericEdit1";
            this.c1NumericEdit1.NumericInput = false;
            this.c1NumericEdit1.Size = new System.Drawing.Size(196, 21);
            this.c1NumericEdit1.TabIndex = 6;
            this.c1NumericEdit1.Tag = null;
            this.c1NumericEdit1.VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1NumericEdit1.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            // 
            // c1TextBox2
            // 
            this.c1TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1TextBox2.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.employeesBindingSource, "LastName", true));
            this.c1TextBox2.DateTimeInput = false;
            this.c1TextBox2.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.FormatType | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.c1TextBox2.Location = new System.Drawing.Point(288, 73);
            this.c1TextBox2.Margin = new System.Windows.Forms.Padding(4);
            this.c1TextBox2.Name = "c1TextBox2";
            this.c1TextBox2.NumericInput = false;
            this.c1TextBox2.Size = new System.Drawing.Size(196, 21);
            this.c1TextBox2.TabIndex = 8;
            this.c1TextBox2.Tag = null;
            this.c1TextBox2.VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1TextBox2.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            // 
            // c1TextBox3
            // 
            this.c1TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1TextBox3.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.employeesBindingSource, "HomePhone", true));
            this.c1TextBox3.DateTimeInput = false;
            this.c1TextBox3.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.FormatType | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.c1TextBox3.EditMask = "(999) 000-0000";
            this.c1TextBox3.Location = new System.Drawing.Point(288, 107);
            this.c1TextBox3.Margin = new System.Windows.Forms.Padding(4);
            this.c1TextBox3.Name = "c1TextBox3";
            this.c1TextBox3.NumericInput = false;
            this.c1TextBox3.Size = new System.Drawing.Size(196, 21);
            this.c1TextBox3.TabIndex = 11;
            this.c1TextBox3.Tag = null;
            this.c1TextBox3.VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1TextBox3.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            // 
            // c1DropDownControl1
            // 
            this.c1DropDownControl1.AutoOpen = false;
            this.c1DropDownControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1DropDownControl1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.employeesBindingSource, "Country", true));
            this.c1DropDownControl1.DateTimeInput = false;
            this.c1DropDownControl1.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.FormatType | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.c1DropDownControl1.DropDownFormAlign = C1.Win.C1Input.DropDownFormAlignmentEnum.Right;
            this.c1DropDownControl1.DropDownFormClassName = "ControlExplorer.Input.DropDowns.ImageMapping";
            this.c1DropDownControl1.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1DropDownControl1.Location = new System.Drawing.Point(288, 209);
            this.c1DropDownControl1.Margin = new System.Windows.Forms.Padding(4);
            this.c1DropDownControl1.Name = "c1DropDownControl1";
            this.c1DropDownControl1.NumericInput = false;
            this.c1DropDownControl1.Size = new System.Drawing.Size(196, 21);
            this.c1DropDownControl1.TabIndex = 12;
            this.c1DropDownControl1.Tag = null;
            this.c1DropDownControl1.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.DropDown;
            this.c1DropDownControl1.VisualStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            this.c1DropDownControl1.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(221, 75);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "姓氏:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(221, 41);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "名字:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(221, 143);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "生日:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(221, 109);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 16;
            this.label4.Text = "电话号码:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(221, 177);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 17);
            this.label5.TabIndex = 17;
            this.label5.Text = "分机号码:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(221, 211);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "国家:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(221, 7);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 17);
            this.label7.TabIndex = 19;
            this.label7.Text = "编号:";
            // 
            // c1Button1
            // 
            this.c1Button1.Image = global::ControlExplorer.Properties.Resources.Save;
            this.c1Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.c1Button1.Location = new System.Drawing.Point(412, 293);
            this.c1Button1.Margin = new System.Windows.Forms.Padding(4);
            this.c1Button1.Name = "c1Button1";
            this.c1Button1.Size = new System.Drawing.Size(72, 33);
            this.c1Button1.TabIndex = 20;
            this.c1Button1.Text = "提交";
            this.c1Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c1Button1.UseVisualStyleBackColor = true;
            this.c1Button1.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Office2007Blue;
            // 
            // employeesTableAdapter
            // 
            this.employeesTableAdapter.ClearBeforeFill = true;
            // 
            // c1PictureBox1
            // 
            this.c1PictureBox1.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.employeesBindingSource, "Photo", true));
            this.c1PictureBox1.Location = new System.Drawing.Point(7, 7);
            this.c1PictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.c1PictureBox1.Name = "c1PictureBox1";
            this.c1PictureBox1.Size = new System.Drawing.Size(207, 227);
            this.c1PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c1PictureBox1.TabIndex = 3;
            this.c1PictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(14, 16);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(500, 413);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tabPage1.Controls.Add(this.c1PictureBox1);
            this.tabPage1.Controls.Add(this.c1Button1);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.c1DbNavigator1);
            this.tabPage1.Controls.Add(this.c1TextBox1);
            this.tabPage1.Controls.Add(this.c1DropDownControl1);
            this.tabPage1.Controls.Add(this.c1DateEdit1);
            this.tabPage1.Controls.Add(this.c1NumericEdit1);
            this.tabPage1.Controls.Add(this.c1TextBox2);
            this.tabPage1.Controls.Add(this.c1Label1);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.c1TextBox3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(492, 383);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "员工";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.c1CheckBox1);
            this.tabPage2.Controls.Add(this.c1TextBox5);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.c1NumericEdit4);
            this.tabPage2.Controls.Add(this.c1NumericEdit3);
            this.tabPage2.Controls.Add(this.c1NumericEdit2);
            this.tabPage2.Controls.Add(this.c1Label2);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.c1TextBox4);
            this.tabPage2.Controls.Add(this.c1DbNavigator2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(492, 383);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "产品";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // c1CheckBox1
            // 
            this.c1CheckBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c1CheckBox1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.productsBindingSource, "Discontinued", true));
            this.c1CheckBox1.Location = new System.Drawing.Point(404, 138);
            this.c1CheckBox1.Margin = new System.Windows.Forms.Padding(4);
            this.c1CheckBox1.Name = "c1CheckBox1";
            this.c1CheckBox1.Size = new System.Drawing.Size(70, 32);
            this.c1CheckBox1.TabIndex = 13;
            this.c1CheckBox1.Text = "已停产";
            this.c1CheckBox1.Value = null;
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "Products";
            this.productsBindingSource.DataSource = this.c1DemoDataSet;
            // 
            // c1TextBox5
            // 
            this.c1TextBox5.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.productsBindingSource, "QuantityPerUnit", true));
            this.c1TextBox5.Location = new System.Drawing.Point(94, 150);
            this.c1TextBox5.Margin = new System.Windows.Forms.Padding(4);
            this.c1TextBox5.Name = "c1TextBox5";
            this.c1TextBox5.Size = new System.Drawing.Size(153, 21);
            this.c1TextBox5.TabIndex = 12;
            this.c1TextBox5.Tag = null;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 153);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 17);
            this.label13.TabIndex = 11;
            this.label13.Text = "每单位数量:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(277, 72);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 17);
            this.label12.TabIndex = 10;
            this.label12.Text = "订购单位:";
            // 
            // c1NumericEdit4
            // 
            this.c1NumericEdit4.AutoOpen = false;
            this.c1NumericEdit4.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.productsBindingSource, "UnitsOnOrder", true));
            this.c1NumericEdit4.DataType = typeof(short);
            this.c1NumericEdit4.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1NumericEdit4.Location = new System.Drawing.Point(344, 69);
            this.c1NumericEdit4.Margin = new System.Windows.Forms.Padding(4);
            this.c1NumericEdit4.Name = "c1NumericEdit4";
            this.c1NumericEdit4.Size = new System.Drawing.Size(130, 21);
            this.c1NumericEdit4.TabIndex = 9;
            this.c1NumericEdit4.Tag = null;
            // 
            // c1NumericEdit3
            // 
            this.c1NumericEdit3.AutoOpen = false;
            this.c1NumericEdit3.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.productsBindingSource, "UnitsInStock", true));
            this.c1NumericEdit3.DataType = typeof(short);
            this.c1NumericEdit3.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1NumericEdit3.Location = new System.Drawing.Point(344, 29);
            this.c1NumericEdit3.Margin = new System.Windows.Forms.Padding(4);
            this.c1NumericEdit3.Name = "c1NumericEdit3";
            this.c1NumericEdit3.Size = new System.Drawing.Size(130, 21);
            this.c1NumericEdit3.TabIndex = 8;
            this.c1NumericEdit3.Tag = null;
            // 
            // c1NumericEdit2
            // 
            this.c1NumericEdit2.AutoOpen = false;
            this.c1NumericEdit2.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.productsBindingSource, "UnitPrice", true));
            this.c1NumericEdit2.FormatType = C1.Win.C1Input.FormatTypeEnum.Currency;
            this.c1NumericEdit2.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1NumericEdit2.Location = new System.Drawing.Point(94, 109);
            this.c1NumericEdit2.Margin = new System.Windows.Forms.Padding(4);
            this.c1NumericEdit2.Name = "c1NumericEdit2";
            this.c1NumericEdit2.Size = new System.Drawing.Size(116, 21);
            this.c1NumericEdit2.TabIndex = 7;
            this.c1NumericEdit2.Tag = null;
            // 
            // c1Label2
            // 
            this.c1Label2.AutoSize = true;
            this.c1Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c1Label2.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.productsBindingSource, "ProductID", true));
            this.c1Label2.DataType = typeof(int);
            this.c1Label2.Location = new System.Drawing.Point(94, 33);
            this.c1Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.c1Label2.Name = "c1Label2";
            this.c1Label2.Size = new System.Drawing.Size(59, 17);
            this.c1Label2.TabIndex = 6;
            this.c1Label2.Tag = null;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(277, 32);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 17);
            this.label11.TabIndex = 5;
            this.label11.Text = "库存单位:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 113);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "单价:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 72);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "产品名称:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 32);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "产品编号:";
            // 
            // c1TextBox4
            // 
            this.c1TextBox4.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.productsBindingSource, "ProductName", true));
            this.c1TextBox4.Location = new System.Drawing.Point(94, 69);
            this.c1TextBox4.Margin = new System.Windows.Forms.Padding(4);
            this.c1TextBox4.Name = "c1TextBox4";
            this.c1TextBox4.Size = new System.Drawing.Size(153, 21);
            this.c1TextBox4.TabIndex = 1;
            this.c1TextBox4.Tag = null;
            // 
            // c1DbNavigator2
            // 
            this.c1DbNavigator2.DataSource = this.productsBindingSource;
            this.c1DbNavigator2.Location = new System.Drawing.Point(8, 320);
            this.c1DbNavigator2.Margin = new System.Windows.Forms.Padding(4);
            this.c1DbNavigator2.Name = "c1DbNavigator2";
            this.c1DbNavigator2.Size = new System.Drawing.Size(413, 22);
            this.c1DbNavigator2.TabIndex = 0;
            this.c1DbNavigator2.VisibleButtons = ((C1.Win.C1Input.NavigatorButtonFlags)(((((((((((C1.Win.C1Input.NavigatorButtonFlags.First | C1.Win.C1Input.NavigatorButtonFlags.Previous) 
            | C1.Win.C1Input.NavigatorButtonFlags.Next) 
            | C1.Win.C1Input.NavigatorButtonFlags.Last) 
            | C1.Win.C1Input.NavigatorButtonFlags.Add) 
            | C1.Win.C1Input.NavigatorButtonFlags.Delete) 
            | C1.Win.C1Input.NavigatorButtonFlags.Edit) 
            | C1.Win.C1Input.NavigatorButtonFlags.Apply) 
            | C1.Win.C1Input.NavigatorButtonFlags.Cancel) 
            | C1.Win.C1Input.NavigatorButtonFlags.Update) 
            | C1.Win.C1Input.NavigatorButtonFlags.Refresh)));
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // c1Magnify1
            // 
            this.c1Magnify1.PopupDelay = 300;
            this.c1Magnify1.Shape = C1.Win.TouchToolKit.MagnifierShape.RoundedRectangle;
            this.c1Magnify1.Size = new System.Drawing.Size(250, 250);
            this.c1Magnify1.ZoomFactor = 3F;
            // 
            // Overview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 718);
            this.Controls.Add(this.tabControl1);
            this.c1Magnify1.SetEnableMagnifier(this, true);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Overview";
            this.Text = "概貌";
            this.Load += new System.EventHandler(this.Overview_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DemoDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DbNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DropDownControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1PictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DbNavigator2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1Input.C1TextBox c1TextBox1;
        private C1.Win.C1Input.C1DateEdit c1DateEdit1;
        private C1.Win.C1Input.C1Label c1Label1;
        private C1.Win.C1Input.C1DbNavigator c1DbNavigator1;
        private C1.Win.C1Input.C1NumericEdit c1NumericEdit1;
        private C1.Win.C1Input.C1TextBox c1TextBox2;
        private C1.Win.C1Input.C1TextBox c1TextBox3;
        private C1.Win.C1Input.C1DropDownControl c1DropDownControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private C1.Win.C1Input.C1Button c1Button1;
        private C1DemoDataSet c1DemoDataSet;
        private System.Windows.Forms.BindingSource employeesBindingSource;
        private ControlExplorer.C1DemoDataSetTableAdapters.EmployeesTableAdapter employeesTableAdapter;
        private C1.Win.C1Input.C1PictureBox c1PictureBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private C1.Win.C1Input.C1TextBox c1TextBox4;
        private C1.Win.C1Input.C1DbNavigator c1DbNavigator2;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private ControlExplorer.C1DemoDataSetTableAdapters.ProductsTableAdapter productsTableAdapter;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private C1.Win.C1Input.C1CheckBox c1CheckBox1;
        private C1.Win.C1Input.C1TextBox c1TextBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private C1.Win.C1Input.C1NumericEdit c1NumericEdit4;
        private C1.Win.C1Input.C1NumericEdit c1NumericEdit3;
        private C1.Win.C1Input.C1NumericEdit c1NumericEdit2;
        private C1.Win.C1Input.C1Label c1Label2;
        private C1.Win.TouchToolKit.C1Magnify c1Magnify1;
    }
}