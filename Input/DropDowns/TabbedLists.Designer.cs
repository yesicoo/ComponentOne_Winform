﻿namespace ControlExplorer.Input.DropDowns
{
    partial class TabbedLists
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c1DockingTab1 = new C1.Win.C1Command.C1DockingTab();
            this.c1DockingTabPage1 = new C1.Win.C1Command.C1DockingTabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.c1DockingTabPage2 = new C1.Win.C1Command.C1DockingTabPage();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.c1DockingTabPage3 = new C1.Win.C1Command.C1DockingTabPage();
            this.listBox3 = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.c1DockingTab1)).BeginInit();
            this.c1DockingTab1.SuspendLayout();
            this.c1DockingTabPage1.SuspendLayout();
            this.c1DockingTabPage2.SuspendLayout();
            this.c1DockingTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // c1DockingTab1
            // 
            this.c1DockingTab1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.c1DockingTab1.Controls.Add(this.c1DockingTabPage1);
            this.c1DockingTab1.Controls.Add(this.c1DockingTabPage2);
            this.c1DockingTab1.Controls.Add(this.c1DockingTabPage3);
            this.c1DockingTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1DockingTab1.Location = new System.Drawing.Point(0, 0);
            this.c1DockingTab1.Name = "c1DockingTab1";
            this.c1DockingTab1.Size = new System.Drawing.Size(195, 238);
            this.c1DockingTab1.TabIndex = 0;
            this.c1DockingTab1.TabsSpacing = 5;
            this.c1DockingTab1.TabStyle = C1.Win.C1Command.TabStyleEnum.WindowsXP;
            this.c1DockingTab1.TextDirection = C1.Win.C1Command.TabTextDirectionEnum.VerticalRight;
            this.c1DockingTab1.VisualStyle = C1.Win.C1Command.VisualStyle.WindowsXP;
            this.c1DockingTab1.VisualStyleBase = C1.Win.C1Command.VisualStyle.WindowsXP;
            // 
            // c1DockingTabPage1
            // 
            this.c1DockingTabPage1.Controls.Add(this.listBox1);
            this.c1DockingTabPage1.Location = new System.Drawing.Point(26, 2);
            this.c1DockingTabPage1.Name = "c1DockingTabPage1";
            this.c1DockingTabPage1.Size = new System.Drawing.Size(165, 232);
            this.c1DockingTabPage1.TabIndex = 0;
            this.c1DockingTabPage1.Text = "美国";
            // 
            // listBox1
            // 
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.IntegralHeight = false;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Items.AddRange(new object[] {
            "阿拉巴马州",
            "阿拉斯加州",
            "亚利桑那州",
            "阿肯色州",
            "加利福尼亚州",
            "科罗拉多州",
            "康乃迪克州",
            "特拉华州",
            "佛罗里达州",
            "乔治亚州",
            "夏威夷州",
            "爱达荷州",
            "伊利诺伊州",
            "印地安那州",
            "爱荷华州",
            "堪萨斯州",
            "肯塔基州",
            "路易斯安那州",
            "缅因州",
            "马里兰州",
            "马萨诸塞州",
            "密歇根",
            "明尼苏达州",
            "密西西比",
            "密苏里州",
            "蒙大拿",
            "内布拉斯加州",
            "内华达州",
            "新罕布什尔州",
            "新泽西",
            "新墨西哥",
            "纽约",
            "北卡罗莱纳州",
            "北达科他州",
            "俄亥俄州",
            "俄克拉何马",
            "俄勒冈州",
            "宾夕法尼亚州",
            "罗德岛",
            "南卡罗来纳州",
            "南达科他州",
            "田纳西州",
            "得克萨斯州",
            "犹他州",
            "佛蒙特",
            "弗吉尼亚州",
            "华盛顿",
            "西弗吉尼亚州",
            "威斯康星州",
            "怀俄明州"});
            this.listBox1.Location = new System.Drawing.Point(0, 0);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(165, 232);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // c1DockingTabPage2
            // 
            this.c1DockingTabPage2.Controls.Add(this.listBox2);
            this.c1DockingTabPage2.Location = new System.Drawing.Point(26, 2);
            this.c1DockingTabPage2.Name = "c1DockingTabPage2";
            this.c1DockingTabPage2.Size = new System.Drawing.Size(165, 232);
            this.c1DockingTabPage2.TabIndex = 1;
            this.c1DockingTabPage2.Text = "美国领土";
            // 
            // listBox2
            // 
            this.listBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.IntegralHeight = false;
            this.listBox2.ItemHeight = 12;
            this.listBox2.Items.AddRange(new object[] {
            "美属萨摩亚",
            "哥伦比亚特区",
            "关岛",
            "北马里亚纳群岛",
            "波多黎各",
            "维尔京群岛"});
            this.listBox2.Location = new System.Drawing.Point(0, 0);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(165, 232);
            this.listBox2.TabIndex = 0;
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // c1DockingTabPage3
            // 
            this.c1DockingTabPage3.Controls.Add(this.listBox3);
            this.c1DockingTabPage3.Location = new System.Drawing.Point(26, 2);
            this.c1DockingTabPage3.Name = "c1DockingTabPage3";
            this.c1DockingTabPage3.Size = new System.Drawing.Size(165, 232);
            this.c1DockingTabPage3.TabIndex = 2;
            this.c1DockingTabPage3.Text = "加拿大";
            // 
            // listBox3
            // 
            this.listBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox3.FormattingEnabled = true;
            this.listBox3.IntegralHeight = false;
            this.listBox3.ItemHeight = 12;
            this.listBox3.Items.AddRange(new object[] {
            "阿尔伯塔",
            "不列颠哥伦比亚",
            "马尼托巴",
            "新不伦瑞克",
            "纽芬兰和拉布拉多",
            "西北地区",
            "新斯科舍省",
            "努勒维特",
            "安大略湖",
            "爱德华王子岛",
            "魁北克",
            "萨斯喀彻温省"});
            this.listBox3.Location = new System.Drawing.Point(0, 0);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(165, 232);
            this.listBox3.TabIndex = 0;
            this.listBox3.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // TabbedLists
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(195, 238);
            this.Controls.Add(this.c1DockingTab1);
            this.Name = "TabbedLists";
            this.Text = "标签列表";
            ((System.ComponentModel.ISupportInitialize)(this.c1DockingTab1)).EndInit();
            this.c1DockingTab1.ResumeLayout(false);
            this.c1DockingTabPage1.ResumeLayout(false);
            this.c1DockingTabPage2.ResumeLayout(false);
            this.c1DockingTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1Command.C1DockingTab c1DockingTab1;
        private C1.Win.C1Command.C1DockingTabPage c1DockingTabPage1;
        private System.Windows.Forms.ListBox listBox1;
        private C1.Win.C1Command.C1DockingTabPage c1DockingTabPage2;
        private System.Windows.Forms.ListBox listBox2;
        private C1.Win.C1Command.C1DockingTabPage c1DockingTabPage3;
        private System.Windows.Forms.ListBox listBox3;
    }
}