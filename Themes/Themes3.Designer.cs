﻿namespace ControlExplorer.Themes
{
    partial class Themes3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Themes3));
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            this.c1SuperTooltip1 = new C1.Win.C1SuperTooltip.C1SuperTooltip(this.components);
            this.c1FlexGrid1 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.c1Ribbon1 = new C1.Win.C1Ribbon.C1Ribbon();
            this.ribbonApplicationMenu1 = new C1.Win.C1Ribbon.RibbonApplicationMenu();
            this.ribbonConfigToolBar1 = new C1.Win.C1Ribbon.RibbonConfigToolBar();
            this.ribbonQat1 = new C1.Win.C1Ribbon.RibbonQat();
            this.tab1 = new C1.Win.C1Ribbon.RibbonTab();
            this.ClipboardGroup = new C1.Win.C1Ribbon.RibbonGroup();
            this.PasteSplitButton = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.PasteButton = new C1.Win.C1Ribbon.RibbonButton();
            this.PasteAsTextButton = new C1.Win.C1Ribbon.RibbonButton();
            this.CutButton = new C1.Win.C1Ribbon.RibbonButton();
            this.CopyButton = new C1.Win.C1Ribbon.RibbonButton();
            this.FormatPainterButton = new C1.Win.C1Ribbon.RibbonButton();
            this.FontGroup = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonToolBar1 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.FontFaceComboBox = new C1.Win.C1Ribbon.RibbonFontComboBox();
            this.FontSizeComboBox = new C1.Win.C1Ribbon.RibbonComboBox();
            this.ribbonToolBar2 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.FontBoldButton = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.FontItalicButton = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.FontUnderlineButton = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.FontStrikeoutButton = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.ribbonSeparator1 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.FontColorPicker = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.BackColorPicker = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.ParagraphGroup = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonToolBar31 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.DecreaseIndentButton1 = new C1.Win.C1Ribbon.RibbonButton();
            this.IncreaseIndentButton1 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonToolBar4 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.ribbonToggleGroup1 = new C1.Win.C1Ribbon.RibbonToggleGroup();
            this.ParagraphAlignLeftButton = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.ParagraphAlignCenterButton = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.ParagraphAlignRightButton = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.ProofingGroup = new C1.Win.C1Ribbon.RibbonGroup();
            this.btnSpelling = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonTab2 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup21 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton2 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton1 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup2 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton3 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton4 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonToolBar3 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.DecreaseIndentButton = new C1.Win.C1Ribbon.RibbonButton();
            this.IncreaseIndentButton = new C1.Win.C1Ribbon.RibbonButton();
            this.c1CommandHolder1 = new C1.Win.C1Command.C1CommandHolder();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1CommandHolder1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1ThemeController1
            // 
            this.c1ThemeController1.Theme = "(none)";
            // 
            // c1SuperTooltip1
            // 
            this.c1SuperTooltip1.Font = new System.Drawing.Font("Tahoma", 8F);
            this.c1ThemeController1.SetTheme(this.c1SuperTooltip1, "(default)");
            // 
            // c1FlexGrid1
            // 
            this.c1FlexGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.c1FlexGrid1.BackColor = System.Drawing.Color.White;
            this.c1FlexGrid1.ColumnInfo = "10,1,0,0,0,105,Columns:1{Caption:\"员工号\";}\t2{Caption:\"姓名\";}\t3{Caption:\"入职时间\";}\t4{Ca" +
                "ption:\"职位\";}\t5{Caption:\"薪水\";}\t";
            this.c1FlexGrid1.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1FlexGrid1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.c1FlexGrid1.Location = new System.Drawing.Point(0, 176);
            this.c1FlexGrid1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1FlexGrid1.Name = "c1FlexGrid1";
            this.c1FlexGrid1.Rows.DefaultSize = 21;
            this.c1FlexGrid1.ShowThemedHeaders = C1.Win.C1FlexGrid.ShowThemedHeadersEnum.None;
            this.c1FlexGrid1.Size = new System.Drawing.Size(706, 392);
            this.c1FlexGrid1.StyleInfo = resources.GetString("c1FlexGrid1.StyleInfo");
            this.c1FlexGrid1.TabIndex = 2;
            this.c1ThemeController1.SetTheme(this.c1FlexGrid1, "(default)");
            // 
            // c1Ribbon1
            // 
            this.c1Ribbon1.ApplicationMenuHolder = this.ribbonApplicationMenu1;
            this.c1Ribbon1.ConfigToolBarHolder = this.ribbonConfigToolBar1;
            this.c1Ribbon1.Location = new System.Drawing.Point(0, 0);
            this.c1Ribbon1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Ribbon1.Name = "c1Ribbon1";
            this.c1Ribbon1.QatHolder = this.ribbonQat1;
            this.c1Ribbon1.Size = new System.Drawing.Size(707, 146);
            this.c1Ribbon1.Tabs.Add(this.tab1);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab2);
            this.c1ThemeController1.SetTheme(this.c1Ribbon1, "(default)");
            this.c1Ribbon1.VisualStyle = C1.Win.C1Ribbon.VisualStyle.Custom;
            // 
            // ribbonApplicationMenu1
            // 
            this.ribbonApplicationMenu1.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonApplicationMenu1.LargeImage")));
            this.ribbonApplicationMenu1.Name = "ribbonApplicationMenu1";
            this.ribbonApplicationMenu1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonApplicationMenu1.SmallImage")));
            // 
            // ribbonConfigToolBar1
            // 
            this.ribbonConfigToolBar1.Name = "ribbonConfigToolBar1";
            // 
            // ribbonQat1
            // 
            this.ribbonQat1.Name = "ribbonQat1";
            // 
            // tab1
            // 
            this.tab1.Groups.Add(this.ClipboardGroup);
            this.tab1.Groups.Add(this.FontGroup);
            this.tab1.Groups.Add(this.ParagraphGroup);
            this.tab1.Groups.Add(this.ProofingGroup);
            this.tab1.Name = "tab1";
            this.tab1.Text = "开始";
            // 
            // ClipboardGroup
            // 
            this.ClipboardGroup.Image = ((System.Drawing.Image)(resources.GetObject("ClipboardGroup.Image")));
            this.ClipboardGroup.Items.Add(this.PasteSplitButton);
            this.ClipboardGroup.Items.Add(this.CutButton);
            this.ClipboardGroup.Items.Add(this.CopyButton);
            this.ClipboardGroup.Items.Add(this.FormatPainterButton);
            this.ClipboardGroup.Name = "ClipboardGroup";
            this.ClipboardGroup.Text = "剪切板";
            // 
            // PasteSplitButton
            // 
            this.PasteSplitButton.Items.Add(this.PasteButton);
            this.PasteSplitButton.Items.Add(this.PasteAsTextButton);
            this.PasteSplitButton.LargeImage = ((System.Drawing.Image)(resources.GetObject("PasteSplitButton.LargeImage")));
            this.PasteSplitButton.Name = "PasteSplitButton";
            this.PasteSplitButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("PasteSplitButton.SmallImage")));
            this.PasteSplitButton.Text = "粘贴 ";
            this.PasteSplitButton.ToolTip = resources.GetString("PasteSplitButton.ToolTip");
            // 
            // PasteButton
            // 
            this.PasteButton.Name = "PasteButton";
            this.PasteButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("PasteButton.SmallImage")));
            this.PasteButton.Text = "Paste";
            // 
            // PasteAsTextButton
            // 
            this.PasteAsTextButton.Name = "PasteAsTextButton";
            this.PasteAsTextButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("PasteAsTextButton.SmallImage")));
            this.PasteAsTextButton.Text = "Paste As Text";
            // 
            // CutButton
            // 
            this.CutButton.Name = "CutButton";
            this.CutButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("CutButton.SmallImage")));
            this.CutButton.Text = "剪切";
            this.CutButton.ToolTip = resources.GetString("CutButton.ToolTip");
            // 
            // CopyButton
            // 
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("CopyButton.SmallImage")));
            this.CopyButton.Text = "复制";
            this.CopyButton.ToolTip = resources.GetString("CopyButton.ToolTip");
            // 
            // FormatPainterButton
            // 
            this.FormatPainterButton.Name = "FormatPainterButton";
            this.FormatPainterButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("FormatPainterButton.SmallImage")));
            this.FormatPainterButton.Text = "格式刷";
            this.FormatPainterButton.ToolTip = resources.GetString("FormatPainterButton.ToolTip");
            // 
            // FontGroup
            // 
            this.FontGroup.HasLauncherButton = true;
            this.FontGroup.Image = ((System.Drawing.Image)(resources.GetObject("FontGroup.Image")));
            this.FontGroup.Items.Add(this.ribbonToolBar1);
            this.FontGroup.Items.Add(this.ribbonToolBar2);
            this.FontGroup.Name = "FontGroup";
            this.FontGroup.Text = "字体";
            // 
            // ribbonToolBar1
            // 
            this.ribbonToolBar1.Items.Add(this.FontFaceComboBox);
            this.ribbonToolBar1.Items.Add(this.FontSizeComboBox);
            this.ribbonToolBar1.Name = "ribbonToolBar1";
            // 
            // FontFaceComboBox
            // 
            this.FontFaceComboBox.Name = "FontFaceComboBox";
            this.FontFaceComboBox.TextAreaWidth = 120;
            // 
            // FontSizeComboBox
            // 
            this.FontSizeComboBox.GripHandleVisible = true;
            this.FontSizeComboBox.MaxDropDownItems = 100;
            this.FontSizeComboBox.MaxLength = 3;
            this.FontSizeComboBox.Name = "FontSizeComboBox";
            this.FontSizeComboBox.Text = "12";
            this.FontSizeComboBox.TextAreaWidth = 40;
            // 
            // ribbonToolBar2
            // 
            this.ribbonToolBar2.Items.Add(this.FontBoldButton);
            this.ribbonToolBar2.Items.Add(this.FontItalicButton);
            this.ribbonToolBar2.Items.Add(this.FontUnderlineButton);
            this.ribbonToolBar2.Items.Add(this.FontStrikeoutButton);
            this.ribbonToolBar2.Items.Add(this.ribbonSeparator1);
            this.ribbonToolBar2.Items.Add(this.FontColorPicker);
            this.ribbonToolBar2.Items.Add(this.BackColorPicker);
            this.ribbonToolBar2.Name = "ribbonToolBar2";
            // 
            // FontBoldButton
            // 
            this.FontBoldButton.Name = "FontBoldButton";
            this.FontBoldButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("FontBoldButton.SmallImage")));
            this.FontBoldButton.ToolTip = resources.GetString("FontBoldButton.ToolTip");
            // 
            // FontItalicButton
            // 
            this.FontItalicButton.Name = "FontItalicButton";
            this.FontItalicButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("FontItalicButton.SmallImage")));
            this.FontItalicButton.ToolTip = resources.GetString("FontItalicButton.ToolTip");
            // 
            // FontUnderlineButton
            // 
            this.FontUnderlineButton.Name = "FontUnderlineButton";
            this.FontUnderlineButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("FontUnderlineButton.SmallImage")));
            this.FontUnderlineButton.ToolTip = resources.GetString("FontUnderlineButton.ToolTip");
            // 
            // FontStrikeoutButton
            // 
            this.FontStrikeoutButton.Name = "FontStrikeoutButton";
            this.FontStrikeoutButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("FontStrikeoutButton.SmallImage")));
            this.FontStrikeoutButton.ToolTip = resources.GetString("FontStrikeoutButton.ToolTip");
            // 
            // ribbonSeparator1
            // 
            this.ribbonSeparator1.Name = "ribbonSeparator1";
            // 
            // FontColorPicker
            // 
            this.FontColorPicker.Color = System.Drawing.Color.Red;
            this.FontColorPicker.Name = "FontColorPicker";
            this.FontColorPicker.SmallImage = ((System.Drawing.Image)(resources.GetObject("FontColorPicker.SmallImage")));
            this.FontColorPicker.ToolTip = resources.GetString("FontColorPicker.ToolTip");
            // 
            // BackColorPicker
            // 
            this.BackColorPicker.Color = System.Drawing.Color.Yellow;
            this.BackColorPicker.Name = "BackColorPicker";
            this.BackColorPicker.SmallImage = ((System.Drawing.Image)(resources.GetObject("BackColorPicker.SmallImage")));
            this.BackColorPicker.ToolTip = resources.GetString("BackColorPicker.ToolTip");
            // 
            // ParagraphGroup
            // 
            this.ParagraphGroup.Image = ((System.Drawing.Image)(resources.GetObject("ParagraphGroup.Image")));
            this.ParagraphGroup.Items.Add(this.ribbonToolBar31);
            this.ParagraphGroup.Items.Add(this.ribbonToolBar4);
            this.ParagraphGroup.Name = "ParagraphGroup";
            this.ParagraphGroup.Text = "段落";
            // 
            // ribbonToolBar31
            // 
            this.ribbonToolBar31.Items.Add(this.DecreaseIndentButton1);
            this.ribbonToolBar31.Items.Add(this.IncreaseIndentButton1);
            this.ribbonToolBar31.Name = "ribbonToolBar31";
            // 
            // DecreaseIndentButton1
            // 
            this.DecreaseIndentButton1.Name = "DecreaseIndentButton1";
            this.DecreaseIndentButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("DecreaseIndentButton1.SmallImage")));
            this.DecreaseIndentButton1.ToolTip = resources.GetString("DecreaseIndentButton1.ToolTip");
            // 
            // IncreaseIndentButton1
            // 
            this.IncreaseIndentButton1.Name = "IncreaseIndentButton1";
            this.IncreaseIndentButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("IncreaseIndentButton1.SmallImage")));
            this.IncreaseIndentButton1.ToolTip = resources.GetString("IncreaseIndentButton1.ToolTip");
            // 
            // ribbonToolBar4
            // 
            this.ribbonToolBar4.Items.Add(this.ribbonToggleGroup1);
            this.ribbonToolBar4.Name = "ribbonToolBar4";
            // 
            // ribbonToggleGroup1
            // 
            this.ribbonToggleGroup1.Items.Add(this.ParagraphAlignLeftButton);
            this.ribbonToggleGroup1.Items.Add(this.ParagraphAlignCenterButton);
            this.ribbonToggleGroup1.Items.Add(this.ParagraphAlignRightButton);
            this.ribbonToggleGroup1.Name = "ribbonToggleGroup1";
            // 
            // ParagraphAlignLeftButton
            // 
            this.ParagraphAlignLeftButton.Name = "ParagraphAlignLeftButton";
            this.ParagraphAlignLeftButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("ParagraphAlignLeftButton.SmallImage")));
            this.ParagraphAlignLeftButton.ToolTip = resources.GetString("ParagraphAlignLeftButton.ToolTip");
            // 
            // ParagraphAlignCenterButton
            // 
            this.ParagraphAlignCenterButton.Name = "ParagraphAlignCenterButton";
            this.ParagraphAlignCenterButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("ParagraphAlignCenterButton.SmallImage")));
            this.ParagraphAlignCenterButton.ToolTip = resources.GetString("ParagraphAlignCenterButton.ToolTip");
            // 
            // ParagraphAlignRightButton
            // 
            this.ParagraphAlignRightButton.Name = "ParagraphAlignRightButton";
            this.ParagraphAlignRightButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("ParagraphAlignRightButton.SmallImage")));
            this.ParagraphAlignRightButton.ToolTip = resources.GetString("ParagraphAlignRightButton.ToolTip");
            // 
            // ProofingGroup
            // 
            this.ProofingGroup.Items.Add(this.btnSpelling);
            this.ProofingGroup.Name = "ProofingGroup";
            this.ProofingGroup.Text = "校对";
            // 
            // btnSpelling
            // 
            this.btnSpelling.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnSpelling.LargeImage")));
            this.btnSpelling.Name = "btnSpelling";
            this.btnSpelling.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnSpelling.SmallImage")));
            this.btnSpelling.Text = "拼写检查";
            this.btnSpelling.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Groups.Add(this.ribbonGroup21);
            this.ribbonTab2.Groups.Add(this.ribbonGroup2);
            this.ribbonTab2.Name = "ribbonTab2";
            this.ribbonTab2.Text = "插入";
            // 
            // ribbonGroup21
            // 
            this.ribbonGroup21.Items.Add(this.ribbonButton2);
            this.ribbonGroup21.Items.Add(this.ribbonButton1);
            this.ribbonGroup21.Name = "ribbonGroup21";
            this.ribbonGroup21.Text = "页";
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.LargeImage")));
            this.ribbonButton2.Name = "ribbonButton2";
            this.ribbonButton2.Text = "数据透视表";
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.LargeImage")));
            this.ribbonButton1.Name = "ribbonButton1";
            this.ribbonButton1.Text = "表";
            // 
            // ribbonGroup2
            // 
            this.ribbonGroup2.Items.Add(this.ribbonButton3);
            this.ribbonGroup2.Items.Add(this.ribbonButton4);
            this.ribbonGroup2.Name = "ribbonGroup2";
            this.ribbonGroup2.Text = "插图";
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.LargeImage")));
            this.ribbonButton3.Name = "ribbonButton3";
            this.ribbonButton3.Text = "图片";
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.LargeImage")));
            this.ribbonButton4.Name = "ribbonButton4";
            this.ribbonButton4.Text = "形状";
            // 
            // ribbonToolBar3
            // 
            this.ribbonToolBar3.Items.Add(this.DecreaseIndentButton);
            this.ribbonToolBar3.Items.Add(this.IncreaseIndentButton);
            this.ribbonToolBar3.Name = "ribbonToolBar3";
            // 
            // DecreaseIndentButton
            // 
            this.DecreaseIndentButton.Name = "DecreaseIndentButton";
            this.DecreaseIndentButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("DecreaseIndentButton.SmallImage")));
            this.DecreaseIndentButton.ToolTip = resources.GetString("DecreaseIndentButton.ToolTip");
            // 
            // IncreaseIndentButton
            // 
            this.IncreaseIndentButton.Name = "IncreaseIndentButton";
            this.IncreaseIndentButton.SmallImage = ((System.Drawing.Image)(resources.GetObject("IncreaseIndentButton.SmallImage")));
            this.IncreaseIndentButton.ToolTip = resources.GetString("IncreaseIndentButton.ToolTip");
            // 
            // c1CommandHolder1
            // 
            this.c1CommandHolder1.Owner = this;
            // 
            // Themes3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 569);
            this.Controls.Add(this.c1FlexGrid1);
            this.Controls.Add(this.c1Ribbon1);
            this.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.Name = "Themes3";
            this.Text = "Themes3";
            this.c1ThemeController1.SetTheme(this, "(default)");
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1CommandHolder1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;
        private C1.Win.C1Ribbon.C1Ribbon c1Ribbon1;
        private C1.Win.C1Ribbon.RibbonApplicationMenu ribbonApplicationMenu1;
        private C1.Win.C1Ribbon.RibbonConfigToolBar ribbonConfigToolBar1;
        private C1.Win.C1Ribbon.RibbonQat ribbonQat1;
        private C1.Win.C1Ribbon.RibbonTab tab1;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab2;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup2;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup21;
        private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar3;
        private C1.Win.C1Ribbon.RibbonButton DecreaseIndentButton;
        private C1.Win.C1Ribbon.RibbonButton IncreaseIndentButton;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton2;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton1;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton3;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton4;
        private C1.Win.C1Command.C1CommandHolder c1CommandHolder1;
        private C1.Win.C1FlexGrid.C1FlexGrid c1FlexGrid1;
        private C1.Win.C1SuperTooltip.C1SuperTooltip c1SuperTooltip1;
        private C1.Win.C1Ribbon.RibbonGroup ClipboardGroup;
        private C1.Win.C1Ribbon.RibbonSplitButton PasteSplitButton;
        private C1.Win.C1Ribbon.RibbonButton PasteButton;
        private C1.Win.C1Ribbon.RibbonButton PasteAsTextButton;
        private C1.Win.C1Ribbon.RibbonButton CutButton;
        private C1.Win.C1Ribbon.RibbonButton CopyButton;
        private C1.Win.C1Ribbon.RibbonButton FormatPainterButton;
        private C1.Win.C1Ribbon.RibbonGroup FontGroup;
        private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar1;
        private C1.Win.C1Ribbon.RibbonFontComboBox FontFaceComboBox;
        private C1.Win.C1Ribbon.RibbonComboBox FontSizeComboBox;
        private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar2;
        private C1.Win.C1Ribbon.RibbonToggleButton FontBoldButton;
        private C1.Win.C1Ribbon.RibbonToggleButton FontItalicButton;
        private C1.Win.C1Ribbon.RibbonToggleButton FontUnderlineButton;
        private C1.Win.C1Ribbon.RibbonToggleButton FontStrikeoutButton;
        private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator1;
        private C1.Win.C1Ribbon.RibbonColorPicker FontColorPicker;
        private C1.Win.C1Ribbon.RibbonColorPicker BackColorPicker;
        private C1.Win.C1Ribbon.RibbonGroup ParagraphGroup;
        private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar31;
        private C1.Win.C1Ribbon.RibbonButton DecreaseIndentButton1;
        private C1.Win.C1Ribbon.RibbonButton IncreaseIndentButton1;
        private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar4;
        private C1.Win.C1Ribbon.RibbonToggleGroup ribbonToggleGroup1;
        private C1.Win.C1Ribbon.RibbonToggleButton ParagraphAlignLeftButton;
        private C1.Win.C1Ribbon.RibbonToggleButton ParagraphAlignCenterButton;
        private C1.Win.C1Ribbon.RibbonToggleButton ParagraphAlignRightButton;
        private C1.Win.C1Ribbon.RibbonGroup ProofingGroup;
        private C1.Win.C1Ribbon.RibbonButton btnSpelling;
    }
}