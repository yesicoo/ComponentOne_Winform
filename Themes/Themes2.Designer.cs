﻿namespace ControlExplorer.Themes
{
    partial class Themes2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            this.c1Button1 = new C1.Win.C1Input.C1Button();
            this.c1TextBox1 = new C1.Win.C1Input.C1TextBox();
            this.c1DropDownControl1 = new C1.Win.C1Input.C1DropDownControl();
            this.c1DateEdit1 = new C1.Win.C1Input.C1DateEdit();
            this.c1DbNavigator1 = new C1.Win.C1Input.C1DbNavigator();
            this.c1Label1 = new C1.Win.C1Input.C1Label();
            this.c1NumericEdit1 = new C1.Win.C1Input.C1NumericEdit();
            this.c1CheckBox1 = new C1.Win.C1Input.C1CheckBox();
            this.c1SplitButton1 = new C1.Win.C1Input.C1SplitButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DropDownControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DbNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1ThemeController1
            // 
            this.c1ThemeController1.Theme = "(none)";
            // 
            // c1Button1
            // 
            this.c1Button1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1Button1.Location = new System.Drawing.Point(19, 4);
            this.c1Button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Button1.Name = "c1Button1";
            this.c1Button1.Size = new System.Drawing.Size(87, 28);
            this.c1Button1.TabIndex = 0;
            this.c1Button1.Text = "c1Button1";
            this.c1Button1.UseVisualStyleBackColor = true;
            // 
            // c1TextBox1
            // 
            this.c1TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1TextBox1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1TextBox1.Location = new System.Drawing.Point(135, 4);
            this.c1TextBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1TextBox1.Name = "c1TextBox1";
            this.c1TextBox1.Size = new System.Drawing.Size(117, 20);
            this.c1TextBox1.TabIndex = 1;
            this.c1TextBox1.Tag = null;
            // 
            // c1DropDownControl1
            // 
            this.c1DropDownControl1.AutoOpen = false;
            this.c1DropDownControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1DropDownControl1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1DropDownControl1.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1DropDownControl1.Location = new System.Drawing.Point(19, 39);
            this.c1DropDownControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1DropDownControl1.Name = "c1DropDownControl1";
            this.c1DropDownControl1.Size = new System.Drawing.Size(233, 20);
            this.c1DropDownControl1.TabIndex = 2;
            this.c1DropDownControl1.Tag = null;
            // 
            // c1DateEdit1
            // 
            this.c1DateEdit1.AutoOpen = false;
            this.c1DateEdit1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1DateEdit1.Calendar.DayNameLength = 2;
            this.c1DateEdit1.Calendar.Font = new System.Drawing.Font("Tahoma", 8F);
            this.c1DateEdit1.Calendar.VisualStyle = C1.Win.C1Input.VisualStyle.System;
            this.c1DateEdit1.Calendar.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Custom;
            this.c1DateEdit1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1DateEdit1.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1DateEdit1.Location = new System.Drawing.Point(19, 80);
            this.c1DateEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1DateEdit1.Name = "c1DateEdit1";
            this.c1DateEdit1.Size = new System.Drawing.Size(233, 20);
            this.c1DateEdit1.TabIndex = 3;
            this.c1DateEdit1.Tag = null;
            // 
            // c1DbNavigator1
            // 
            this.c1DbNavigator1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1DbNavigator1.ForeColor = System.Drawing.Color.Black;
            this.c1DbNavigator1.Location = new System.Drawing.Point(19, 145);
            this.c1DbNavigator1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1DbNavigator1.Name = "c1DbNavigator1";
            this.c1DbNavigator1.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.c1DbNavigator1.Size = new System.Drawing.Size(273, 37);
            this.c1DbNavigator1.TabIndex = 4;
            // 
            // c1Label1
            // 
            this.c1Label1.AutoSize = true;
            this.c1Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c1Label1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1Label1.ForeColor = System.Drawing.Color.Black;
            this.c1Label1.Location = new System.Drawing.Point(19, 122);
            this.c1Label1.Name = "c1Label1";
            this.c1Label1.Size = new System.Drawing.Size(51, 13);
            this.c1Label1.TabIndex = 5;
            this.c1Label1.Tag = null;
            // 
            // c1NumericEdit1
            // 
            this.c1NumericEdit1.AutoOpen = false;
            this.c1NumericEdit1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // 
            // 
            this.c1NumericEdit1.Calculator.BackColor = System.Drawing.SystemColors.Control;
            this.c1NumericEdit1.Calculator.VisualStyle = C1.Win.C1Input.VisualStyle.System;
            this.c1NumericEdit1.Calculator.VisualStyleBaseStyle = C1.Win.C1Input.VisualStyle.Custom;
            this.c1NumericEdit1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1NumericEdit1.ImagePadding = new System.Windows.Forms.Padding(0);
            this.c1NumericEdit1.Location = new System.Drawing.Point(19, 202);
            this.c1NumericEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1NumericEdit1.Name = "c1NumericEdit1";
            this.c1NumericEdit1.Size = new System.Drawing.Size(233, 20);
            this.c1NumericEdit1.TabIndex = 6;
            this.c1NumericEdit1.Tag = null;
            // 
            // c1CheckBox1
            // 
            this.c1CheckBox1.BackColor = System.Drawing.Color.Transparent;
            this.c1CheckBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c1CheckBox1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1CheckBox1.ForeColor = System.Drawing.Color.Black;
            this.c1CheckBox1.Location = new System.Drawing.Point(19, 245);
            this.c1CheckBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1CheckBox1.Name = "c1CheckBox1";
            this.c1CheckBox1.Padding = new System.Windows.Forms.Padding(5, 1, 1, 1);
            this.c1CheckBox1.Size = new System.Drawing.Size(121, 30);
            this.c1CheckBox1.TabIndex = 7;
            this.c1CheckBox1.Text = "c1CheckBox1";
            this.c1CheckBox1.UseVisualStyleBackColor = false;
            this.c1CheckBox1.Value = null;
            // 
            // c1SplitButton1
            // 
            this.c1SplitButton1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1SplitButton1.Location = new System.Drawing.Point(19, 282);
            this.c1SplitButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1SplitButton1.Name = "c1SplitButton1";
            this.c1SplitButton1.Size = new System.Drawing.Size(134, 28);
            this.c1SplitButton1.TabIndex = 8;
            this.c1SplitButton1.Text = "c1SplitButton1";
            this.c1SplitButton1.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(238)))), ((int)(((byte)(194)))));
            this.comboBox1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.comboBox1.ForeColor = System.Drawing.Color.Black;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(370, 2);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(140, 21);
            this.comboBox1.TabIndex = 9;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Themes2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 325);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.c1SplitButton1);
            this.Controls.Add(this.c1CheckBox1);
            this.Controls.Add(this.c1NumericEdit1);
            this.Controls.Add(this.c1Label1);
            this.Controls.Add(this.c1DbNavigator1);
            this.Controls.Add(this.c1DateEdit1);
            this.Controls.Add(this.c1DropDownControl1);
            this.Controls.Add(this.c1TextBox1);
            this.Controls.Add(this.c1Button1);
            this.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "Themes2";
            this.Text = "Themes2";
            this.Load += new System.EventHandler(this.Themes2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DropDownControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1DbNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1NumericEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;
        private C1.Win.C1Input.C1Button c1Button1;
        private C1.Win.C1Input.C1TextBox c1TextBox1;
        private C1.Win.C1Input.C1DropDownControl c1DropDownControl1;
        private C1.Win.C1Input.C1DateEdit c1DateEdit1;
        private C1.Win.C1Input.C1DbNavigator c1DbNavigator1;
        private C1.Win.C1Input.C1Label c1Label1;
        private C1.Win.C1Input.C1NumericEdit c1NumericEdit1;
        private C1.Win.C1Input.C1CheckBox c1CheckBox1;
        private C1.Win.C1Input.C1SplitButton c1SplitButton1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}