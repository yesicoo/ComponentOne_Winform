﻿namespace ControlExplorer.SplitContainer
{
    partial class SplitContainerDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c1SplitContainer1 = new C1.Win.C1SplitContainer.C1SplitContainer();
            this.c1SplitterPanel1 = new C1.Win.C1SplitContainer.C1SplitterPanel();
            this.c1SplitterPanel2 = new C1.Win.C1SplitContainer.C1SplitterPanel();
            this.c1SplitContainer2 = new C1.Win.C1SplitContainer.C1SplitContainer();
            this.c1SplitterPanel7 = new C1.Win.C1SplitContainer.C1SplitterPanel();
            this.c1SplitterPanel8 = new C1.Win.C1SplitContainer.C1SplitterPanel();
            this.c1SplitterPanel3 = new C1.Win.C1SplitContainer.C1SplitterPanel();
            this.c1SplitterPanel4 = new C1.Win.C1SplitContainer.C1SplitterPanel();
            this.c1SplitterPanel5 = new C1.Win.C1SplitContainer.C1SplitterPanel();
            this.c1SplitterPanel6 = new C1.Win.C1SplitContainer.C1SplitterPanel();
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            ((System.ComponentModel.ISupportInitialize)(this.c1SplitContainer1)).BeginInit();
            this.c1SplitContainer1.SuspendLayout();
            this.c1SplitterPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1SplitContainer2)).BeginInit();
            this.c1SplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1SplitContainer1
            // 
            this.c1SplitContainer1.BackColor = System.Drawing.Color.Gray;
            this.c1SplitContainer1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(101)))), ((int)(((byte)(189)))));
            this.c1SplitContainer1.CollapsingAreaColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(237)))), ((int)(((byte)(255)))));
            this.c1SplitContainer1.CollapsingCueColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(101)))), ((int)(((byte)(189)))));
            this.c1SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1SplitContainer1.FixedLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(101)))), ((int)(((byte)(189)))));
            this.c1SplitContainer1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1SplitContainer1.ForeColor = System.Drawing.Color.White;
            this.c1SplitContainer1.HeaderForeColor = System.Drawing.Color.White;
            this.c1SplitContainer1.HeaderLineWidth = 1;
            this.c1SplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.c1SplitContainer1.Name = "c1SplitContainer1";
            this.c1SplitContainer1.Panels.Add(this.c1SplitterPanel1);
            this.c1SplitContainer1.Panels.Add(this.c1SplitterPanel2);
            this.c1SplitContainer1.Panels.Add(this.c1SplitterPanel3);
            this.c1SplitContainer1.Panels.Add(this.c1SplitterPanel4);
            this.c1SplitContainer1.Panels.Add(this.c1SplitterPanel5);
            this.c1SplitContainer1.Panels.Add(this.c1SplitterPanel6);
            this.c1SplitContainer1.Size = new System.Drawing.Size(591, 446);
            this.c1SplitContainer1.SplitterColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(101)))), ((int)(((byte)(189)))));
            this.c1SplitContainer1.SplitterMovingColor = System.Drawing.Color.Black;
            this.c1SplitContainer1.TabIndex = 0;
            this.c1ThemeController1.SetTheme(this.c1SplitContainer1, "(default)");
            this.c1SplitContainer1.UseParentVisualStyle = false;
            // 
            // c1SplitterPanel1
            // 
            this.c1SplitterPanel1.Collapsible = true;
            this.c1SplitterPanel1.Dock = C1.Win.C1SplitContainer.PanelDockStyle.Left;
            this.c1SplitterPanel1.Location = new System.Drawing.Point(0, 21);
            this.c1SplitterPanel1.Name = "c1SplitterPanel1";
            this.c1SplitterPanel1.Size = new System.Drawing.Size(168, 425);
            this.c1SplitterPanel1.SizeRatio = 29.863D;
            this.c1SplitterPanel1.TabIndex = 0;
            this.c1SplitterPanel1.Text = "面板 1";
            this.c1SplitterPanel1.Width = 175;
            // 
            // c1SplitterPanel2
            // 
            this.c1SplitterPanel2.Collapsible = true;
            this.c1SplitterPanel2.Controls.Add(this.c1SplitContainer2);
            this.c1SplitterPanel2.Dock = C1.Win.C1SplitContainer.PanelDockStyle.Right;
            this.c1SplitterPanel2.Height = 133;
            this.c1SplitterPanel2.Location = new System.Drawing.Point(454, 0);
            this.c1SplitterPanel2.Name = "c1SplitterPanel2";
            this.c1SplitterPanel2.Size = new System.Drawing.Size(137, 446);
            this.c1SplitterPanel2.SizeRatio = 35.381D;
            this.c1SplitterPanel2.TabIndex = 1;
            this.c1SplitterPanel2.Width = 144;
            // 
            // c1SplitContainer2
            // 
            this.c1SplitContainer2.BackColor = System.Drawing.Color.Gray;
            this.c1SplitContainer2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(101)))), ((int)(((byte)(189)))));
            this.c1SplitContainer2.CollapsingAreaColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(237)))), ((int)(((byte)(255)))));
            this.c1SplitContainer2.CollapsingCueColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(101)))), ((int)(((byte)(189)))));
            this.c1SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1SplitContainer2.FixedLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(101)))), ((int)(((byte)(189)))));
            this.c1SplitContainer2.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.c1SplitContainer2.ForeColor = System.Drawing.Color.White;
            this.c1SplitContainer2.HeaderForeColor = System.Drawing.Color.White;
            this.c1SplitContainer2.HeaderLineWidth = 1;
            this.c1SplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.c1SplitContainer2.Name = "c1SplitContainer2";
            this.c1SplitContainer2.Panels.Add(this.c1SplitterPanel7);
            this.c1SplitContainer2.Panels.Add(this.c1SplitterPanel8);
            this.c1SplitContainer2.Size = new System.Drawing.Size(137, 446);
            this.c1SplitContainer2.SplitterColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(101)))), ((int)(((byte)(189)))));
            this.c1SplitContainer2.SplitterMovingColor = System.Drawing.Color.Black;
            this.c1SplitContainer2.TabIndex = 0;
            this.c1ThemeController1.SetTheme(this.c1SplitContainer2, "(default)");
            this.c1SplitContainer2.UseParentVisualStyle = false;
            // 
            // c1SplitterPanel7
            // 
            this.c1SplitterPanel7.Collapsible = true;
            this.c1SplitterPanel7.Height = 221;
            this.c1SplitterPanel7.Location = new System.Drawing.Point(0, 21);
            this.c1SplitterPanel7.Name = "c1SplitterPanel7";
            this.c1SplitterPanel7.Size = new System.Drawing.Size(137, 193);
            this.c1SplitterPanel7.TabIndex = 0;
            this.c1SplitterPanel7.Text = "面板 2";
            // 
            // c1SplitterPanel8
            // 
            this.c1SplitterPanel8.Collapsible = true;
            this.c1SplitterPanel8.Height = 100;
            this.c1SplitterPanel8.Location = new System.Drawing.Point(0, 246);
            this.c1SplitterPanel8.Name = "c1SplitterPanel8";
            this.c1SplitterPanel8.Size = new System.Drawing.Size(137, 200);
            this.c1SplitterPanel8.TabIndex = 1;
            this.c1SplitterPanel8.Text = "面板 7";
            // 
            // c1SplitterPanel3
            // 
            this.c1SplitterPanel3.Collapsible = true;
            this.c1SplitterPanel3.Height = 135;
            this.c1SplitterPanel3.Location = new System.Drawing.Point(179, 21);
            this.c1SplitterPanel3.Name = "c1SplitterPanel3";
            this.c1SplitterPanel3.Size = new System.Drawing.Size(264, 107);
            this.c1SplitterPanel3.SizeRatio = 30.455D;
            this.c1SplitterPanel3.TabIndex = 2;
            this.c1SplitterPanel3.Text = "面板 3";
            // 
            // c1SplitterPanel4
            // 
            this.c1SplitterPanel4.Collapsible = true;
            this.c1SplitterPanel4.Height = 157;
            this.c1SplitterPanel4.Location = new System.Drawing.Point(179, 160);
            this.c1SplitterPanel4.Name = "c1SplitterPanel4";
            this.c1SplitterPanel4.Size = new System.Drawing.Size(264, 129);
            this.c1SplitterPanel4.SizeRatio = 51.656D;
            this.c1SplitterPanel4.TabIndex = 3;
            this.c1SplitterPanel4.Text = "面板 4";
            // 
            // c1SplitterPanel5
            // 
            this.c1SplitterPanel5.Collapsible = true;
            this.c1SplitterPanel5.Dock = C1.Win.C1SplitContainer.PanelDockStyle.Right;
            this.c1SplitterPanel5.Location = new System.Drawing.Point(320, 321);
            this.c1SplitterPanel5.Name = "c1SplitterPanel5";
            this.c1SplitterPanel5.Size = new System.Drawing.Size(123, 125);
            this.c1SplitterPanel5.TabIndex = 4;
            this.c1SplitterPanel5.Text = "面板 5";
            this.c1SplitterPanel5.Width = 130;
            // 
            // c1SplitterPanel6
            // 
            this.c1SplitterPanel6.Collapsible = true;
            this.c1SplitterPanel6.Height = 100;
            this.c1SplitterPanel6.Location = new System.Drawing.Point(179, 321);
            this.c1SplitterPanel6.Name = "c1SplitterPanel6";
            this.c1SplitterPanel6.Size = new System.Drawing.Size(130, 125);
            this.c1SplitterPanel6.TabIndex = 5;
            this.c1SplitterPanel6.Text = "面板 6";
            // 
            // SplitContainerDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 446);
            this.Controls.Add(this.c1SplitContainer1);
            this.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "SplitContainerDemo";
            this.Text = "SplitContainerDemo";
            this.c1ThemeController1.SetTheme(this, "(default)");
            ((System.ComponentModel.ISupportInitialize)(this.c1SplitContainer1)).EndInit();
            this.c1SplitContainer1.ResumeLayout(false);
            this.c1SplitterPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1SplitContainer2)).EndInit();
            this.c1SplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1SplitContainer.C1SplitContainer c1SplitContainer1;
        private C1.Win.C1SplitContainer.C1SplitterPanel c1SplitterPanel1;
        private C1.Win.C1SplitContainer.C1SplitterPanel c1SplitterPanel2;
        private C1.Win.C1SplitContainer.C1SplitterPanel c1SplitterPanel3;
        private C1.Win.C1SplitContainer.C1SplitterPanel c1SplitterPanel4;
        private C1.Win.C1SplitContainer.C1SplitterPanel c1SplitterPanel5;
        private C1.Win.C1SplitContainer.C1SplitterPanel c1SplitterPanel6;
        private C1.Win.C1SplitContainer.C1SplitContainer c1SplitContainer2;
        private C1.Win.C1SplitContainer.C1SplitterPanel c1SplitterPanel7;
        private C1.Win.C1SplitContainer.C1SplitterPanel c1SplitterPanel8;
        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;

    }
}